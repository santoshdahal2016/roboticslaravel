<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTableRainy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->integer('active')->default(0);
            $table->integer('phone')->default();
            $table->text('faculty')->nullable();
            $table->text('year')->nullable();
            $table->integer('sessions_id')->nullable();


        });

        // users table
        Schema::create('sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->text('slogan');
            $table->text('slogan_owner');
            $table->timestamps();
        });

        Schema::create('sessions_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('sessions')->onDelete('cascade');
            $table->integer('project_id')->unsigned();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('active');
            $table->dropColumn('phone');
            $table->dropColumn('faculty');
            $table->dropColumn('year');
        });

        Schema::dropIfExists('sessions');

    }
}
