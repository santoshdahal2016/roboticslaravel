<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->timestamps();
        });
        Schema::create('materials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->integer('source');
            $table->integer('quantity');
            $table->integer('damaged')->default(0);
            $table->integer('material_category_id')->unsigned();
            $table->foreign('material_category_id')->references('id')->on('material_category')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('material_issue', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->integer('materials_id')->unsigned();
            $table->foreign('materials_id')->references('id')->on('materials')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('status');
            $table->date('deadline');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials');
        Schema::dropIfExists('material_issue');
        Schema::dropIfExists('material_category');


    }
}
