-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 13, 2018 at 12:18 AM
-- Server version: 5.7.22-0ubuntu18.04.1
-- PHP Version: 7.2.5-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `robotics`
--

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `conversations`
--

INSERT INTO `conversations` (`id`, `message`, `user_id`, `group_id`, `created_at`, `updated_at`) VALUES
(25, 'Hi every body welcome to project', 2, 1, '2018-02-20 09:37:11', '2018-02-20 09:37:11'),
(28, 'testing', 1, 1, '2018-02-20 16:05:56', '2018-02-20 16:05:56'),
(29, 'Hello', 2, 1, '2018-02-20 16:12:59', '2018-02-20 16:12:59'),
(30, 'Hello World!', 2, 1, '2018-02-20 16:13:15', '2018-02-20 16:13:15'),
(31, 'la bhai maile paye', 1, 1, '2018-02-20 16:13:58', '2018-02-20 16:13:58'),
(32, 'Ahile aaba yo chat kasle kasle herna milxa dai? Yo group ma vako sab le haina', 2, 1, '2018-02-20 16:14:32', '2018-02-20 16:14:32'),
(33, 'aile yo testing matarai ho , yes ma k  feature tapna sakincha bhana hai', 1, 1, '2018-02-20 16:14:45', '2018-02-20 16:14:45'),
(34, 'Laa', 2, 1, '2018-02-20 16:14:56', '2018-02-20 16:14:56'),
(35, 'yo project ma bhako le matra ho ,, aru le mildai naw', 1, 1, '2018-02-20 16:15:19', '2018-02-20 16:15:19'),
(36, 'Side ma emoji thapnu!! Ramailo hunxa hola.. Boring hudaina', 2, 1, '2018-02-20 16:15:23', '2018-02-20 16:15:23'),
(37, 'Side ma emoji thapnu!! Ramailo hunxa hola.. Boring hudaina', 2, 1, '2018-02-20 16:15:25', '2018-02-20 16:15:25'),
(38, 'ok', 1, 1, '2018-02-20 16:15:36', '2018-02-20 16:15:36'),
(39, 'Laa, Yo ni vayo aaba...', 2, 1, '2018-02-20 16:16:27', '2018-02-20 16:16:27'),
(40, 'Laa, Yo ni vayo aaba...', 2, 1, '2018-02-20 16:16:28', '2018-02-20 16:16:28'),
(41, 'ma yesko design pani change garchu , account ma profile bhaye pachi photo pani aaucha', 1, 1, '2018-02-20 16:16:30', '2018-02-20 16:16:30'),
(42, 'Aee', 2, 1, '2018-02-20 16:16:50', '2018-02-20 16:16:50'),
(43, 'aile yo engine matra ho', 1, 1, '2018-02-20 16:17:02', '2018-02-20 16:17:02'),
(44, 'Laa dai ma chai janxu, voli assessment xa padhnu xa', 2, 1, '2018-02-20 16:17:08', '2018-02-20 16:17:08'),
(45, 'ok', 1, 1, '2018-02-20 16:17:14', '2018-02-20 16:17:14'),
(46, 'Bye hai dai, Aaba dekhi yesmai kura gardai garumlaa', 2, 1, '2018-02-20 16:17:25', '2018-02-20 16:17:25'),
(47, 'bye', 1, 1, '2018-02-20 16:17:38', '2018-02-20 16:17:38'),
(48, NULL, 4, 1, '2018-02-20 16:54:43', '2018-02-20 16:54:43'),
(49, 'bijayan', 4, 1, '2018-02-20 16:54:47', '2018-02-20 16:54:47'),
(50, 'hello all of my friend', 4, 1, '2018-02-20 16:54:58', '2018-02-20 16:54:58'),
(51, 'hello all of my friend', 4, 1, '2018-02-20 16:55:00', '2018-02-20 16:55:00'),
(52, 'hi guys', 5, 1, '2018-02-21 03:16:39', '2018-02-21 03:16:39'),
(53, 'hi guys!!', 5, 1, '2018-02-21 03:16:41', '2018-02-21 03:16:41'),
(54, 'https://youtu.be/FaLVXLDszuQ', 4, 1, '2018-02-21 10:08:23', '2018-02-21 10:08:23'),
(55, 'yo geet sunam hai', 4, 1, '2018-02-21 10:08:38', '2018-02-21 10:08:38'),
(56, 'yo geet sunam hai', 4, 1, '2018-02-21 10:08:39', '2018-02-21 10:08:39'),
(57, 'ok', 1, 1, '2018-02-21 10:46:39', '2018-02-21 10:46:39'),
(58, 'Hi guys, Are you there?', 5, 1, '2018-02-21 16:19:38', '2018-02-21 16:19:38'),
(59, 'hi', 1, 2, '2018-02-24 12:39:46', '2018-02-24 12:39:46'),
(60, 'hello', 1, 2, '2018-02-24 12:43:50', '2018-02-24 12:43:50'),
(61, 'jjkhkj', 1, 2, '2018-02-24 12:44:30', '2018-02-24 12:44:30'),
(62, 'testing own chat server', 1, 1, '2018-04-15 17:54:20', '2018-04-15 17:54:20'),
(63, 'learned to use new language node.js , use nosql database (redis) for first time  ever and integrated with laravel,  new chat server is at http://suchana.herokuapp.com/ . very excited', 1, 1, '2018-04-15 17:57:31', '2018-04-15 17:57:31'),
(64, 'testing', 1, 1, '2018-04-16 09:11:33', '2018-04-16 09:11:33'),
(65, '.', 1, 1, '2018-04-16 09:39:58', '2018-04-16 09:39:58'),
(66, 'hi', 1, 2, '2018-05-04 08:58:52', '2018-05-04 08:58:52'),
(67, 'hjhj', 1, 2, '2018-05-04 08:59:02', '2018-05-04 08:59:02'),
(68, 'hjhj', 1, 2, '2018-05-04 08:59:03', '2018-05-04 08:59:03'),
(69, 'sadsad', 1, 2, '2018-05-04 08:59:23', '2018-05-04 08:59:23'),
(70, 'ff', 1, 1, '2018-05-12 07:41:55', '2018-05-12 07:41:55');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, '1', NULL, NULL),
(2, '2', '2018-02-24 12:27:02', '2018-02-24 12:27:02');

-- --------------------------------------------------------

--
-- Table structure for table `group_user`
--

CREATE TABLE `group_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `group_user`
--

INSERT INTO `group_user` (`id`, `group_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 1, 2, NULL, NULL),
(3, 1, 3, NULL, NULL),
(4, 1, 4, NULL, NULL),
(5, 1, 5, NULL, NULL),
(6, 2, 1, '2018-02-24 12:27:02', '2018-02-24 12:27:02');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(20, 'default', '{\"displayName\":\"App\\\\Events\\\\NewMessage\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\",\"command\":\"O:38:\\\"Illuminate\\\\Broadcasting\\\\BroadcastEvent\\\":7:{s:5:\\\"event\\\";O:21:\\\"App\\\\Events\\\\NewMessage\\\":2:{s:12:\\\"conversation\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":3:{s:5:\\\"class\\\";s:36:\\\"App\\\\Modules\\\\Chat\\\\Models\\\\Conversation\\\";s:2:\\\"id\\\";i:27;s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"socket\\\";N;}s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1519141103, 1519141103);

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

CREATE TABLE `materials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `damaged` int(11) NOT NULL DEFAULT '0',
  `material_category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `materials`
--

INSERT INTO `materials` (`id`, `name`, `description`, `source`, `quantity`, `damaged`, `material_category_id`, `created_at`, `updated_at`) VALUES
(1, 'Motor Driver', '<p><span class=\"st\"><span class=\"f\"></span>In this project we will Interface a <em>Fingerprint Sensor</em> Module with <em>Arduino</em> Uno and build a fingerprint based Biometric Security System with door <br></span><br></p>', 1, 6, 3, 1, '2018-05-12 02:35:58', '2018-05-12 11:05:41'),
(2, 'sadad', '<p>asdasds<br></p>', 2, 2, 0, 2, '2018-05-12 07:40:09', '2018-05-12 07:40:09'),
(3, 'sadad', '<p>sadad<br></p>', 1, 6, 0, 2, '2018-05-12 07:55:36', '2018-05-12 07:55:36');

-- --------------------------------------------------------

--
-- Table structure for table `material_category`
--

CREATE TABLE `material_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `material_category`
--

INSERT INTO `material_category` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Driver', '..', '2018-05-12 02:32:25', '2018-05-12 02:32:25'),
(2, 'adsad', 'adasd', '2018-05-12 07:39:54', '2018-05-12 07:39:54'),
(3, 'ssssssssssssss', 'sss', '2018-05-12 10:31:21', '2018-05-12 10:31:21'),
(4, 'ssssssssssssssd', 'sssd', '2018-05-12 10:32:20', '2018-05-12 10:32:20'),
(5, 'ssssssssssssssds', 'sssds', '2018-05-12 10:33:37', '2018-05-12 10:33:37');

-- --------------------------------------------------------

--
-- Table structure for table `material_issue`
--

CREATE TABLE `material_issue` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `materials_id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `deadline` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `material_issue`
--

INSERT INTO `material_issue` (`id`, `user_id`, `materials_id`, `status`, `deadline`, `created_at`, `updated_at`) VALUES
(1, '1', 1, 2, '2018-05-12', '2018-05-11 18:15:00', '2018-05-12 08:43:34'),
(2, '1', 1, 2, '2018-05-13', '2018-05-12 07:32:49', '2018-05-12 08:43:38'),
(3, '3', 1, 2, '2018-05-13', '2018-05-12 07:35:43', '2018-05-12 08:43:40'),
(4, '4', 1, 2, '2018-05-31', '2018-05-12 07:38:39', '2018-05-12 08:43:42'),
(5, '5', 2, 2, '2018-06-30', '2018-05-12 07:40:32', '2018-05-12 08:44:22'),
(6, '4', 1, 2, '2018-05-31', '2018-05-12 07:49:11', '2018-05-12 08:44:14'),
(7, '5', 2, 2, '2018-05-31', '2018-05-12 07:54:36', '2018-05-12 08:59:30'),
(8, '1', 3, 2, '2018-05-31', '2018-05-12 07:55:51', '2018-05-12 08:59:46'),
(9, '2', 3, 2, '2018-05-31', '2018-05-12 07:57:12', '2018-05-12 08:59:47'),
(10, '5', 3, 2, '2018-05-22', '2018-05-12 07:58:57', '2018-05-12 08:59:48'),
(11, '1', 2, 2, '2018-05-31', '2018-05-12 08:44:44', '2018-05-12 08:59:41'),
(12, '1', 1, 2, '2018-05-10', '2018-05-12 09:17:39', '2018-05-12 09:34:10'),
(13, '3', 2, 1, '2018-06-12', '2018-05-12 09:17:59', '2018-05-12 09:59:28'),
(14, '4', 1, 1, '2018-05-31', '2018-05-12 09:34:24', '2018-05-12 09:34:24'),
(15, '1', 2, 1, '2018-06-12', '2018-05-12 09:58:53', '2018-05-12 09:59:11'),
(16, '1', 1, 1, '2018-05-24', '2018-05-12 11:22:44', '2018-05-12 11:22:44');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_11_23_100933_BaseTablesMigration', 1),
(2, '2018_01_11_094554_entrust_setup_tables', 1),
(3, '2018_02_10_075231_create_task_report_table', 1),
(15, '2018_02_20_032741_create_chats_table', 2),
(16, '2018_02_20_055222_create_jobs_table', 3),
(18, '2018_02_24_052320_create_materials_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'user_index', 'User Index', 'User Index', NULL, NULL),
(2, 'user_create', 'User Create', 'User Create', NULL, NULL),
(3, 'user_store', 'User Store', 'User Store', NULL, NULL),
(4, 'user_show', 'User Show', 'User Show', NULL, NULL),
(5, 'user_edit', 'User Edit', 'User Edit', NULL, NULL),
(6, 'user_update', 'User Update', 'User Update', NULL, NULL),
(7, 'user_destroy', 'User Destroy', 'User Destroy', NULL, NULL),
(8, 'user_role_index', 'User Role Index', 'User Role Index', NULL, NULL),
(9, 'user_role_create', 'User Role Create', 'User Role Create', NULL, NULL),
(10, 'user_role_store', 'User Role Store', 'User Role Store', NULL, NULL),
(11, 'user_role_edit', 'User Role Edit', 'User Role Edit', NULL, NULL),
(12, 'user_role_update', 'User Role Update', 'User Role Update', NULL, NULL),
(13, 'user_role_destroy', 'User Role Destroy', 'User Role Destroy', NULL, NULL),
(14, 'user_permission_index', 'User Permission Index', 'User Permission Index', NULL, NULL),
(15, 'user_permission_create', 'User Permission Create', 'User Permission Create', NULL, NULL),
(16, 'user_permission_store', 'User Permission Store', 'User Permission Store', NULL, NULL),
(17, 'user_permission_edit', 'User Permission Edit', 'User Permission Edit', NULL, NULL),
(18, 'user_permission_update', 'User Permission Update', 'User Permission Update', NULL, NULL),
(19, 'user_permission_destroy', 'User Permission Destroy', 'User Permission Destroy', NULL, NULL),
(20, 'user_permission_search', 'User Permission Search', 'User Permission Search', NULL, NULL),
(21, 'dashboard', 'Dashboard', 'Dashboard', NULL, NULL),
(22, 'material_index', 'Material Index', 'Material Index', '2018-05-12 12:14:31', '2018-05-12 12:14:31'),
(23, 'project', 'Project', 'Project', '2018-05-12 12:16:29', '2018-05-12 12:16:29'),
(24, 'material_create', 'Material Create', 'Material Create', '2018-05-12 12:23:40', '2018-05-12 12:23:40'),
(25, 'material_update', 'Material Update', 'Material Update', '2018-05-12 12:24:32', '2018-05-12 12:24:32'),
(26, 'material_issue', 'Material Issue', 'Material Issue', '2018-05-12 12:25:13', '2018-05-12 12:25:13');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(21, 2),
(23, 2),
(1, 3),
(2, 3),
(3, 3),
(4, 3),
(5, 3),
(6, 3),
(7, 3),
(8, 3),
(9, 3),
(10, 3),
(11, 3),
(12, 3),
(13, 3),
(14, 3),
(15, 3),
(16, 3),
(17, 3),
(18, 3),
(19, 3),
(20, 3),
(21, 3),
(22, 3),
(24, 3),
(25, 3),
(26, 3),
(1, 4),
(2, 4),
(3, 4),
(4, 4),
(5, 4),
(6, 4),
(7, 4),
(21, 4),
(22, 4),
(24, 4),
(25, 4),
(26, 4);

-- --------------------------------------------------------

--
-- Table structure for table `phases`
--

CREATE TABLE `phases` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `projects_id` int(10) UNSIGNED NOT NULL,
  `sequence` int(10) UNSIGNED NOT NULL,
  `deadline` date NOT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'started',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `phases`
--

INSERT INTO `phases` (`id`, `name`, `description`, `projects_id`, `sequence`, `deadline`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Adding  Automation and image processing', 'We are using matlab for this intellegence', 1, 1, '2018-02-13', 'finished', '2018-02-10 03:00:00', '2018-02-10 00:00:09'),
(2, 'Build Mechanical Structure', 'Build 4 dof robot arm', 1, 0, '2018-02-01', 'finished', '2018-02-10 00:00:00', '2018-02-10 00:00:06'),
(3, 'Advance in control and Mechanical Design', 'Show other what they have not though of you', 1, 2, '2018-02-01', 'running', '2018-02-10 00:00:00', '2018-02-10 00:00:06'),
(4, 'Develop core', 'Develop admin panel design plus user management ', 2, 0, '2018-02-01', 'finished', '2018-02-10 00:00:00', '2018-02-10 00:00:06'),
(5, 'Project Management System', 'Develop system which automate project development ', 2, 1, '2018-02-01', 'running', '2018-02-10 00:00:00', '2018-02-10 00:00:06'),
(6, 'Material Management system', 'Keep track of every material of robotics club and develop system to block marksheet without clearance', 2, 2, '2018-02-01', 'running', '2018-02-10 00:00:00', '2018-02-10 00:00:06'),
(7, 'Frontend engine development', 'Develop Curd for fronted content', 2, 3, '2018-02-01', 'starting', '2018-02-10 00:00:00', '2018-02-10 00:00:06'),
(8, 'Account Management System', 'Keep track of money', 2, 4, '2018-02-01', 'starting', '2018-02-10 00:00:00', '2018-02-10 00:00:06');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `description`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Robotics Arm', '<p>This is prototype of industrial robotics arm<br></p>', 1, '2018-01-31 08:28:14', '2018-02-10 08:28:14'),
(2, 'Robot Club Management System plus website', '<p>Let automate robotics club and really focus on projects<br></p>', 1, '2018-02-24 12:27:01', '2018-02-24 12:27:01');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'developer', 'Developer', 'Developer', NULL, '2018-05-12 12:01:42'),
(2, 'member', 'Member', 'Member', NULL, NULL),
(3, 'cithead', 'CIT HEAD', 'CIT HEAD', '2018-05-12 12:00:09', '2018-05-12 12:00:09'),
(4, 'subhod', 'SUB HOD', 'SUB HOD', '2018-05-12 12:00:46', '2018-05-12 12:00:46');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(7, 3),
(8, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phases_id` int(10) UNSIGNED NOT NULL,
  `sequence` int(10) UNSIGNED NOT NULL,
  `deadline` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `name`, `description`, `phases_id`, `sequence`, `deadline`, `created_at`, `updated_at`) VALUES
(1, 'LCD interfacing Arduino with the help of Matlab ', '.', 1, 1, '2018-02-13', '2018-02-10 00:08:00', '2018-02-10 00:00:10'),
(2, 'Maintain Stablity of arm', 'Maintain Stablity of arm (i.e CG , drilling hole , consulting mechanical teacher) And design gripper ', 1, 2, '2018-02-13', '2018-02-10 02:00:00', '2018-02-10 00:00:00'),
(3, 'Inverse kinematics', '.Stored trajectory control using inverse kinematics', 1, 3, '2018-02-13', '2018-02-10 00:08:00', '2018-02-10 00:00:10'),
(4, ' Image Processing', '. Image Processing to detect object in its area', 1, 1, '2018-02-13', '2018-02-10 00:08:00', '2018-02-10 00:00:10'),
(5, 'Use AVR', 'Replace arduino with avr', 3, 1, '2018-02-13', '2018-02-10 00:08:00', '2018-02-10 00:00:10'),
(6, 'Create function for material ', 'complete Feature to create material with category', 6, 1, '2018-02-13', '2018-02-10 00:08:00', '2018-02-10 00:00:10'),
(7, 'Curd For event ', 'Create edit delete function for event', 7, 1, '2018-02-13', '2018-02-10 00:08:00', '2018-02-10 00:00:10');

-- --------------------------------------------------------

--
-- Table structure for table `task_report`
--

CREATE TABLE `task_report` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `users_tasks_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `task_report`
--

INSERT INTO `task_report` (`id`, `name`, `description`, `users_tasks_id`, `created_at`, `updated_at`) VALUES
(1, 'object detection', 'Object detection and co-ordinate finding finish using matlab<br>', 1, '2018-02-10 00:00:00', '2018-02-10 15:42:52'),
(2, 'Research', 'Started online searching for suitable design of end effector<p>                    </p>', 2, '2018-02-10 00:00:00', '2018-02-10 15:30:04'),
(3, 'Interfacing', 'LCD interfaced with arduino<br><p>                    </p>', 4, '2018-02-10 00:00:00', '2018-02-10 15:45:43'),
(5, 'Studying', 'Studying forward and inverse kinematics theory.<br><p><span style=\"font-weight: bold;\"></span>Preparing for assessment, <span style=\"background-color: rgb(0, 255, 0);\">Working after 2:30.</span>&nbsp; </p><p>Gained insight on DH parameters of the transformation matrix in forward kinematics.</p><p>Heading for inverse kinematics,</p><p>The <span style=\"background-color: rgb(0, 255, 255);\">joint instability </span>can be reduced by employing circular revolute contact of child link to parent link. .<span style=\"background-color: rgb(0, 255, 255);\"> Ashish vai and Aplesh vai,<span style=\"background-color: rgb(255, 255, 255);\"> study the mechanics.</span></span><br></p><p>&nbsp;For now, <span style=\"background-color: rgb(8, 82, 148);\">trying to get some sleep</span>. &nbsp;&nbsp; </p><p><br></p>', 5, '2018-02-10 15:54:14', NULL),
(6, 'Mobile', '<p>Position detect in photo</p><p><span style=\"background-color: rgb(0, 0, 0);\"><span style=\"background-color: rgb(255, 0, 0);\">PROBLEM </span>:</span>&nbsp; Ground servo motor damaged&nbsp; <a href=\"http://www.rccrawler.com/forum/electronics/142369-how-diagnose-servo-failures-hopefully-repair-them.html\">Trobelshooting </a></p><p><span style=\"background-color: yellow;\">Base Stepper motor :&nbsp;</span><a href=\"http://www.circuitmagic.com/arduino/how-to-run-a-stepper-motor-with-an-arduino-l293d-ic/\" target=\"_blank\">stepper motor interfacing</a><br><br></p>', 1, '2018-02-11 00:49:08', '2018-02-11 14:48:20'),
(7, 'Got Dispaly', '<p>Successfully LCD (20*4) interfaced with the help of MATLAB and got the display in all 4 Rows.</p><p><br></p>', 4, '2018-02-11 03:18:21', '2018-02-11 12:51:58'),
(8, 'Studying the best book for Robotics', '<p>Introduction to robotics&gt;&nbsp;&nbsp; JJ Craig                                </p>', 5, '2018-02-11 05:39:14', '2018-02-11 05:39:18'),
(9, 'Continuing Research', '<p>Further Research</p><p>End effectors design</p><p><img src=\"http://thnet.co.uk/thnet/robots/Effec.gif\" style=\"width: 426px;\"><br></p>', 2, '2018-02-12 02:17:17', '2018-02-12 02:48:29'),
(10, 'Enough!!! Jumping to real task.', 'Hands on to Matlab implementation,', 5, '2018-02-12 04:36:26', '2018-02-12 17:56:48'),
(11, 'Administration Work', 'Busy with administration work. Uploaded code on git.<br>', 4, '2018-02-12 13:04:00', '2018-02-12 13:45:54'),
(12, 'stepper motor interfacing', '<p><span style=\"background-color: rgb(0, 255, 0); color: rgb(247, 247, 247);\">done open loop control in arduino based on <span style=\"font-weight: bold;\">angle<br></span></span></p><p><span style=\"color: rgb(247, 247, 247); font-weight: 700; background-color: rgb(0, 0, 255);\">Found use full research artical on robot arm :&nbsp;&nbsp;</span><a href=\"https://s3-us-west-1.amazonaws.com/disneyresearch/wp-content/uploads/20150122064211/A-Passively-Safe-and-Gravity-Counterbalanced-Anthropomorphic-Robot-Arm-Paper.pdf\">A passively safe and gravity-counterbalanced anthropomorphic robot arm</a></p>', 1, '2018-02-12 13:23:26', '2018-02-12 17:45:24'),
(13, 'Aplesh kumar mahato', '<p>it is the ball and bearing system i think we should deploy in it.<img src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUSExMVFRUXFxUVFhcVFxcVFRcVFRUWFhUVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODUtNygtLisBCgoKDg0OGhAQGi0dHR0tLS0tLS0tLS0tLS0tLSstLS0tLS0tLS0tLS0tLS0tKy0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAADAQIEBQYAB//EAEAQAAIBAgQDBgMHAQYFBQAAAAECAAMRBBIhMQVBUQYTImFxgTKRoUJSscHR4fAjFTNicqLxBxRDU4IWNGOS0v/EABkBAAMBAQEAAAAAAAAAAAAAAAABAgMEBf/EACkRAQEAAgEDAwMEAwEAAAAAAAABAhEDEiExE0FRMmFxBIGx0SKRoUL/2gAMAwEAAhEDEQA/AMlSEODGIIQzza9KGkxaIgy8PTW8DFFOCxNKTEWKyxbNEVSBC4dTzhShhUWGwbaMYaQ2WKUiCOi9Zxpk7GPcgRGqWjA9MRhp6x9DXWFtEZAIjR94MCABanAHDyYTBLVF7RkCaOkLQS0KYhaAEBiloIvI3fm9oaCbGlY1HikxBwWITEzGKYAkG5gDiTmtDMLxgCsTyiUfOSmpC2kBUXSMFBjWEYDaK50iI28j1TaGgK7COEYJ0cIkYKBzik3Ecu1o6nTiAC0DeTKa2iNCoIrThGr2h6LX1id0I9TaIz7xS0YTH5YA6MUG8Ko0iqIBGencxtSheTcseFhsI9FLCPqGFkas2kDMFaEFybAEny1huF8GeoczeFfqfQdPOaGnTpUdNB5nc+81x4rfsxz5ZPHdT4fgdRtTZR56n5SfQ7PUhq2Zj62H0lzTqg6i1o5hOjHixjny5cqhpw6mNMg+V/xhRhU+6vyEIevKO15S+mI6qjtg6Z+wp66CQsRwKgxvkt6Ej6Ay2J+cbFcYcyvyoKnZ8D4G9m1+olfiME6HxLp1Go/aa9b8xI+JqINCdTM8uHG+OzTHmynlkMsY5mgxHDVfVfCfp8pS4mgVNmFpz5YXF0Y5zJASlc3h2SKDHEyVmxtrxWnRBEqU7GOy6QxEaBGQCrI1WjJxWCKxwIoWdCERYyIoh6cYohFMkHWj1iCOiMWNvOzaQmFwjvtt1MclvgrZPJgMKDJS8HfqvzhF4O/3llenl8J9TH5QlacWlgvBG++Iv9hH74+UfpZfA9XH5QabW3itVliOBH/uD5fvF/sL/wCT/T+8PRy+C9XH5VeeXXCuE/bqDXkvT184XBcIVWzE5rbXFtZb26Tbj4dd6y5OXfaIPFqhp0mYbgEzCYoVqpzPUK7EBTfcX1IOp1noWNw3eIyE2uLXmfr4HIFSpQZ7aK1JsuZb6BvA2vn9JrlL7MpZ7oHZTFVO87tiSvIny/2mzQX3lXw7hjB+9dRTAFlprrYeZ5nf1v7S2YXjkTszNynHwiw26RxcDcwBxaDdh9IwKQBrFsDrBJikOzfXSEDAkWN4ALGVMtN2HJSfkJhTVqVGzMxU3NrE6a9QZ6DiEDoybXBBt5i0zNXg7U7XU1B1Xn6jlFZdf4+Tln/oDgWNqMxRtbWsee9rH5TQYnBhwQR/v6yLgOFkMajWW/ICWYH7QsErHY7CNSax1B2P5HzjBNXjMOHFjMviaJRsp9vMTk5OPp7zw6+Pk6u18o7HWKYpE4GZtDTB3hbiCZxAEMY8UPEaACKzopnRhymEWBzRQ8SdpCGPvIga0XOYaPaxwlDvGA+c0VOmFAA0AlBwF/6ntLTjebuWy/TedXDJ07c3NbvR/wDalPNlzC/rJefS4E88OCCotUPckknW1rbDzJm44CWNFc2/n0m1jHaSMUekYcaZJanfaDfDiLVG4AeIESXg8Tn9pCOGlhgqQUWhJQmKJ1eqALk7TibSs7Rgmg+W9wOX1joiO/amkGtY26jWXmHxCuoZDcHYiePNX5k/sJs+ymOZMKAVJZmbJvcg2A033/GTNitTi8ctMXY3PSVWLxdQozllpIATdzlJHloT72t5yhfiNf8A5grQo9/UT4hoyrrbMx1FwfUD1kTivEmxCVUemVroQz073zBScwB5HQ6W/fTSdi4btDTL5qlKq9O9s2UqhJ0BDElregEsOJ8Zp0kBSlTdqmlM3bQHTMWZtLGwudJhUx2MNPu87mjlC2ZwaVswIspNlI02Ek8RRWw6L3gDLfQncXFr/K9vPygF0eIVKDUziFo1UclSKVbOAdNCyMbHUHl7zSVa1JFLmqaI+znOdGvsBsV+ZnluAwrMw/qL8Vz0152OnsJccZ4kFr5qtNK1NFyItTNkAC3F8pBubjnygG54Z2kVviII2zKQy/MbbzQUq6sNDPK+zvEu8xivSphAwsyL8G1msCSStr733M2HfrS8aOtgfHSvquts1Py6r8ukWjaRjf0kfGYgU0Z2NlAi4bE5wGUzP9s6bOlNb2p5mL+bAEqPfX3tJtVIEvbGiWy38pLxyd8lxuNQf5ymYxfDA6ZVW5AJ05AC5PsLmXnZRXNFc3sT0mc/yi/pqCTp5yPWrWllxmhlbMBofxlHWQmc1mrp143c2U1dJFzE85LpUNIKvR6Qhm4ZjzklmkejSMkhdIqDM0ScVnQBl4jMY3NHBoIPRtNYt4hF4CuSu0egvOAn+p7TTCxFj9ZkuzNS9X2mvUgzq4fpc3L9SAeB0c2fuxfeWKIBbp5RV6RRtYmasjrW1EhYziVNNyPc6St4vxYj+mm/4CUtTCJbPWa/qdIyW1ftNR6j2Mk4LtPTY629plH4phB4Rl+X5wVWlTYZqZHqIrBt6fQxCuAVsRHEmeb8G409FwCbrfX+dZ6Hh8QHUMDe9rQNX4/g+FXNUeimmt7flK6lUUAVGdEd75FvbKig7DkbX9rnnHdqMXmdaKm1zc+g/n0mCocbSjVZno06rAgL3veXQC/w5WW2nU/nHCO7S4Gojs6nMr6qwN1IJuSG2zbCC4dXqJU7wk1HK5QNfulbeYAkzh/FmHegKctQk01OupYAH6n1l3wfhFhmbVjqx/IeUm1UinThD1WzudTyUD6t/PWS17Np92/qW/WatKAW2w/nSArY+kvP30A9rydW+57kZbEdmFtpmXoQx/BriVGMwValqbutrE21A81N7jzE9EoYunU+FvrOxeCUqdOZt+0c3C7Vh0xyLQZqVw7WVm0zBNbhcoAA0tpIuK4lhjTtSotSqgqc+fvMxOjA+EZR6X6ecP2g4UaDZ1HgJ8Q6Hrbl5iHwHCMOE/5ioxykgBFHjdj9kEm3vaX5T4ars5iSiU2vdKlwQfstdrD0IFx79RNJisOtWmUbZhb9CPOY6vxLugKdbC1KCuCtMkndSMhAYDYhZpuE4oPSV+o+vORZqqiI/BHIytXYqbAgKoJA5MQLn3lpTpKihRsBaP8APUfnBZsxPLzgaJxjD5qZPTX5fz6zNqgM11UAgi3KY+rdWI6Ej6zn5p326eHLtoj07aidUpx61LwtKoD4T7TlzyuPdsjBI0yRXplfSRnMqXfeAxjrOgyJ0YRXcQVStbaNxKkayJVrzSTbPaZSxRvrJeYGU4qgyRh652hcRK0fZ/8AvfaatV5zIdm3/q+016+U6OH6XNy/UNa/rI3EauRT1tJF5Wccb+nebMlHSHxVG8zMvXL4pySSKYNgBz9poMcx7lrfdMocFiHRUVDlvuRod9r8pOe/EPEGth6SGxQ/+VxGLTA8dE2tuvIyVisczKyVSTl+G+tjzAPTy8pX8F1Zhy1mdx6e8qt7WQGdQw5/SbTsdjiaZQ622mU4VT8Df5mt6S77PXVmtNvM2gyviUFetUqNZUCr1JJ1so66SsxCYSswdlrUcx0ZlUoxv0BuN99tZD4ijO1Yj7LnfYHkTflyv5iRanFcViLJVqO6hgV7wlyLAiyA9dL6a6QC+w3DwMTl1IVQbnUkt8J9LazXYdABtfprbl16TN9m6TCo4b4gFvz0y3A9LGafEfCetvykqZ7iGONSoaYbKqC7kafW/wC/pqRVtxajT8KU766lizk+ouAD6QWOBAqC4zEgmxuLd5pe3mZkiD98gxXKQSWtph69Grfux3dXUqAdHO5GuzEa2Okvuz/EDUTK+hG/yvaeb8KZxVBvc6W/zBhl+s3HAzfEPb4d/mzXP4/KOXfgtLXjeCFSmwOoIOvOed08+Q0wSDTY69Dtc9ByvPVMUBkOnI7zAcPrJTr1qji6i+g5nWwlQlRQNeoyq5LAvm1Ia5HRrk2+nOelcAXuwUBvoCPdfF/qzTNPjagpGulXD3tmNBAGOW9rsAultPtHfaXXAcetZg6i16aZhyDXckDy1EnJWLRNc2ta3OK5voDrBs+wA0jjYDQRG7y6c5kOKsBWcX5g/MCa8gb218p552uxVFa7g3FSy/hpJyw6uy8c+juMzHlOp1pm8PxIg/FLvAYlKm5sZll+nvt3aY/qMb57LrDYoHwtt1g8bh8uo1EBUoldeXWHwmKHwNsZw5Y3ju5/pv8AeIJM6Sq3DTc2OnKdL9XD5G1W7XEhvg7wtNDeTAJvvSPKmfCkSVhxlEmQdVbyuradaWPZc/1vYza3+sxHZinav7GbUWE6OPw5+TyIum0Bj6OdCIa8dv8AnNGbJFORHlKDEUO6NjfLe6sOV+Rm04nw+92X3lBWTkduhis2JdMvjMKzal7iPwaWGWmLk6X5CXDYGnzpj6wlGgANAAPKT0X3p3L4FwtMIgXoP4ZecFw3hLdZAwWCZyLjSaqhQAUAS6TEUaqUsRX7z4bB7czpaw94WhiXFI4hKGFVNwuZO9y3IJtmz+vh5QfbXBWdXGx8B99Vv9ZmcNwyuagXxAWKgoGLEG+mg1v/ADpANhwriCvWFQDKHVdL81UAzWEC2+/y2mTwvB+7o5SyLVT+oPELjqh9uXW/WX3CcctRLN8Q0IMnWj2z3F8EysWtdTfraxHiU22B1IPIk9QZQ1uDUyxu2U7kPmRh62RlProT0E9NqUQ2lgRIVbhYOgzZdfCdtdxbb5R6G2LwXDwougDEfa8QRTt8TAF35AWAF9L7jV8BwOQFiLFtutthf8dObGS6fDVBBF2I2vfQdBf8JPVdPT6Rki8QcJTa50AJ+kw2E4U9VKj2+JtCdibZrE+9poOMYnvXFJbkEgG3M9P1MzWO4ki1e7rd41Fc3gpsEzN1uQQAbDkYwqqvZ+sHtlbLsb8tdQGGh/ebXslhsmceYGm3wg6TPYHjFMYj+lmFBluyE5shttcAAm+twBNhwKie76MbsfUm5EjKqkWwe2l94vw3v/POcj2tfecNyCNPOIzqZvZvxnjfbWpmxta3IqPki/neewVtiR9J4nxUl61SoftOx9r6fS0PceyCt4SniWUxwSx9Y7EUCNesrZai54f2kYeFzcecvKVZXF1nn5Fpouz1U2ve4G4mfJxzKbnlXHyXC/ZrqWPIAE6NGCY6gaGdPOv6fH4dm8VbedeRATCrWnRpGxbxhM7PEgKtezX997fnNex8/eY3s61q3tL7jtYiixW97Tp4vpc/J5WK4yntnF/WSARynn1Th4XDpX70Go7E5Q3iRV0GbzJ5dJq+A1maipbe00ZrN/KQ8Tg0bcSXacwjJUtwdOph6HCVHL5w9U85Lwla4gCUsOBoIYjlHOh5Su49iGp0WK/FY69IjRePYWm6GmzgEg7kXHMG3raYqpxOpTovTBK1AchI0bIfiKnzH4yqqIWY5tSTck6k+8tsLws1qBa/jQlVJ5gWOU/OLehpBxHFcOaQVKBp1VCnvQ5YsdmuCAEGt9PrvNbwjDGpSWpfJUAObMbKbE6AnS9hsfbpMtw/BU2fKwVGB18ID3v97mISnxSi7t/zLVwi6U0olQAosNcx1562v6SibuhxVlFqi3/xDUSSvFaX39+Ux/ZrigFWqmZalEahqoJsoO99GGh5GWWM4mpDVaeEq9wCAamQZN8psza78idNoaC7q8YpjQEsei6yLUqVqpygFF3PXL1JOijzMg4jjNBKXeUqZ8Y8BLm/PNewFrWN9JAxfFq1KnmarQq0i3jp0XL5SR4czAmzWB+RgFkuLIdqOFois9irMTZEW4ucxtcm4vtvaZvE4dcRUqU2RUqp8S6lGA0up0KsPXaVXaDDtnzKLhtQeRB1Gp2PLlJHC6FTvswfO+XKX1KqCMtgSAW08vwvC0SJ/C+FA1AoAypYtlva/IEkknr/ALzaiqKakk2CjU8pC4TghTX9fqSesh9rFZlSmCFVixYk2BKi6qT5kfO0z3tpIM3a3D6A3/nnLnA4sVB4Z5jwfg+JqFlam2xJuLAADMWJOgAAJmq7Iu4pi97flygS949iO7oseZFh6nSef1cIGWW3a3i+auKAPwgM3+Y7D2Gv/kJAom9h1mHLdV0ccnSz2NwpS15ZJhs+HvzH5R/aZRmVR6mTODf+2f3m3FlcuLqrns1nplK1GP4XXNN/I6GTcbRtK5tAZfHdnyTT0Dh3HgKag8hb6m06ZHCOcg/nOdL1HLcqtEfWLUS8iYdiTJ6icN7PSDVbQgaI63iAWgEvhuLFOoGO2xmvRlqJcHMpmEaSeH8VejoNV6H8pphnrszzw20adn6N81j1ty+UusPRAAA0EyB7WOP+mPmZw7a23o/Jv2mszjK4VuVp+YnNT6GYgdv1/wCy3swjh/xCpc6NX/T+srqhdNa58PfcwSLlN5mR/wAQaB/6dUey/wD6jT29w53Sr8l/WGy02qOIzF4cOCpH85zO8E7XUKz92Cyk7Bha9uh6+U0wPMGMM3/6VBOjkL0FvoZZpw5KaBFFgPxkrF4tUQsdgL6TA8X7Z1CR3eWmvK/iZvPyEXYLTjPBlc3Fww2Ybj35jymUxvC3VvGneDqCwPuFN7eU1XAe0gr2p1Fs/UaqfPyltiMED0vyh3ng9bYSmafdOikBiLW1Ftb28RJvpuTIVOriPhFTwZWF2cFbG97oxuDrsBfp1m1r8HV91B9QDITdnkv8A9iR+Bh1fYdKlOGJwwUnxXZvEdfFb66XlVQwlyQWLn7qq2/+I3tabGlwFL/APe5/GT6PDwumUAfIRdXxB0qahhatQBT4EAAtuxt1Owl9w7h6oAFEld2EUsbWAJPkALn85kuJ9rWvlpWpjqRmYjqRso+sXe+VePDd0U01nYzBLUQow8x+UznAe0RqMKdUeLkw2N9jaarNcRkp6nAqjJ3bV6hT7tzYjoeo8onE66YPDtUb7Isq82b7IEuK9dKSF3YKACSToABuZ492p4+2Kq6X7pbhB16uR1P0HvKJEXFN3pqMbliWY+ust8FWrf3ioMo66Snwou6gi2q3vzBl/wBocUadqQFhbS2x9Jhzy2zGTz/C8LZLv2VGN4h3jFjvNBRBp4QdW/PWZajQJYE8zNHi8TmCryAmuWWOOHTCxxtz7q7FNcSpqyzxrW05yAlLMwHzj4prHfyfLl3/AAtcJR8C+k6W+F4HXdA6r4Tt6DT8p0025uhEw6iTNJAw/WSlOs8+vSggEXu40m05akR6NdJFqCSneBqaxylYhVGkeoJKqpAssuIqE1OCcSbUEjss0lRYjZYhEMRBkStp0apIIINiNQRuCNiJ6L2R7WipajXaz7Btg/6N5Tzq0SMtPaOP0s1BgutxpbWYYdnUrKCaqUWUBWVza9vtC+49ILs92yalanXBentm3cev3h9fWbShTw+IC1EZWHUH6HmPQxxN7M/wHg4WuuQ5kUAZrEXOpJAPK5mw7si99j84enTUCyi0G4vceWspIWT5fzS0aw00115QtrWHLa84nL/tAwxTvfr/AD6RKS6eK1/OPygeK56zgA1jeAR+JUC1KooGpVgPO4ImNodnqFVrvXGHZQgYVQSGIGrJYbabbzfU25QGM4dTY3YDla8Wj2yXDOGBcQTTLPSBAV2XIWHM5eQuTbna17TZYnFpSQsxAAGpMpeL9oMPhhluGf7i2J9+QHrMHxbjFXEtdzZR8KD4R+p84rdHJtM7TcZOJ8AJWkDtza3NvLylXw/DBS2l/AbX6wYELSexmHJbcbGuOOrsOqoekKo+JdGH4RBjnZQG8Q5X3EZhjldqbbPp+hghQIJHMcp08eUkuN8eYWc3ZnPxfzEmlUsbkGSlxJPwqb+chUar7A+xhmd+bWk5TV74xWPR83/hta41OrHlL3s5wumo72uQF3tzPlM+jBTfcw6VXqHU6DlJ68r/AH/TPPGbk1+39thW7YMCRTUBBoo8hFmbWnOnP3+W/TEhIpeDQwloqcFveKogw0UPEbqhgQ0fVMApj0D6gkaqIaoYJkjhWI7iR3EsCsC6CVKm4q9zGGTKlGM7oS+qI6KiGJaSWoxypDrHQiimYfCPUptmpsyHqpt8+vvCgRYuun6caThvbSsthVUOBzGh+W34TQ4ftdh33JU/4hb67TzoCIwjmdF449Zp8QpOPDUU+4h0rr94flPHbR4Y9T85XqJ9J681VBqXXla/11kPEcaw6fFVUeQI/CeWsSd9fWNMXqUek3eP7d0Vv3aFz1tlHzP6TK8U7V4itcZsi9EJv/8Abf5WlO4vGoOsfVuF06ptooqEQrUukYaRi3Kdxsd35nd4YqpDBYdj7mYhS4B5idXYkB9jsfUbGSqCTsSgsZEuuzTGeZ8/yiCrfca9YqqxOlzDYLHWGUqGtCPiGbQDKJrMsp29vuwyty/KIRy5yxwai2kbh6NpLRJPJySzUi8MLLun2iRbGJMGwSPrDgzp0qxMcGnO06dEZwOkE8WdCGW0YYs6ADYQJnToyMqrBWnTo4C2jSJ06ANMUTp0A68QtEnQIs4GdOgZbxDrOnQBgSMrLOnSp5TZ2IhMIHnTo7ChwMZUedOkzyq+C0qhhC06dC+RPCMwysG5HQyWxsVA5xZ0rzpPiX9k+ksMIk6ZNDp06dAP/9k=\" data-filename=\"download (1).jpg\" style=\"width: 275px;\"></p>', 3, '2018-02-12 13:51:29', NULL),
(14, 'Interfacing plus  programming', 'Today  i have interface a stepper motor and three servo motor attached in arm and successfully run code that simultaneously control all of them', 1, '2018-02-14 13:24:19', NULL),
(15, 'Mechanical Bug Fix', 'Base Servo motor is replaced by Steeper Motor. Fix the Spring Problem. Done base fixing', 4, '2018-02-14 13:36:41', '2018-02-14 13:37:23'),
(16, 'Missed deadline.', 'Failed to implement Inverse Kinematics.\r\n\r\nContinuing Modeling the robot for Fk and Ik. Heading to trajectory generation. Will focus on Point To Point trajectory.', 5, '2018-02-15 00:39:41', '2018-02-15 00:42:04'),
(17, 'Trajectory generation', 'robot is able to follow certain step', 1, '2018-02-15 10:08:15', NULL),
(18, 'Mechanical work', 'Busy with mechanical work.', 4, '2018-02-15 12:14:47', NULL),
(19, 'Setup of the stall', 'Bannerr fixing.', 4, '2018-02-16 17:52:00', NULL),
(20, 'New idea of ARM', 'ARM in Stage Management. Stage Inaguration<br>', 4, '2018-02-20 16:45:40', '2018-02-20 16:46:44'),
(21, 'Motivating', 'now motivating&nbsp; all project member .<br>', 4, '2018-02-23 14:22:04', '2018-02-23 14:27:17'),
(22, 'Learning to interface Servo Motor with AVR', '<p><a href=\"https://www.newbiehack.com/MicrocontrollerControlAHobbyServo.aspx\">MicrocontrollerControlAHobbyServo &nbsp; &nbsp;</a>                                </p>', 6, '2018-02-24 12:18:25', '2018-02-24 12:19:30'),
(23, 'Finished', 'Today i have design database and implemented it . Also finish Create feature for Material with Ajax category feature&nbsp; . It has been deployed', 7, '2018-02-24 12:38:46', NULL),
(24, 'Busy With Exam', 'Now time to read for exam', 4, '2018-03-05 02:47:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `report` text COLLATE utf8mb4_unicode_ci,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `report`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Santosh Dahal', 'santosh@robotics.wrc.edu.np', 'No report', '$2y$10$GXL8b3.TYkeUqnSToDACNOc1PK8SO.UDG/xec9i1dtNEB/TqIFt.a', 'Yvk2kqNEwdQNM0ShUQDFCZmkgX9Ew5GoLOAJa6La20wHzVuKOSwrcFXu12TO', '2018-02-10 00:00:00', '2018-05-12 12:47:23'),
(2, 'Aaceesh Bhattrai', 'aaceesh@robotics.wrc.edu.np', 'No report', '$2y$10$bdd6JyRdD9T1Ad8j8uIfzeHUGPr5Vz4/qRuFEypHmpM6kZV1p/lAK', 'YVF5zYHJEpROGugmCq7VcGvsYbIHt1cyVOLfhKcX2NqQq8D5GD19xCRGVbtd', '2018-02-10 00:00:07', '2018-02-10 00:00:07'),
(3, 'Aplesh Mahato', 'aplesh@robotics.wrc.edu.np', 'No report', '$2y$10$bdd6JyRdD9T1Ad8j8uIfzeHUGPr5Vz4/qRuFEypHmpM6kZV1p/lAK', 'iThKf8RAHE7wIeQaoSivh5A80Cox5Rlxo8YqDwhCRYEYvqH256JFbCe0sVKD', '2018-02-10 00:00:07', '2018-02-10 00:00:07'),
(4, 'Bijayan', 'bijayan@robotics.wrc.edu.np', 'No report', '$2y$10$bdd6JyRdD9T1Ad8j8uIfzeHUGPr5Vz4/qRuFEypHmpM6kZV1p/lAK', 'j88Ys7fLsfL7ytraBRiAaYGoLhIFlOrgFYWi94ExrlSpBxtLPETBuQyOrWlQ', '2018-02-10 00:00:07', '2018-02-10 00:00:07'),
(5, 'Siddhant Baral', 'siddhant@robotics.wrc.edu.np', 'No report', '$2y$10$bdd6JyRdD9T1Ad8j8uIfzeHUGPr5Vz4/qRuFEypHmpM6kZV1p/lAK', 'UlOVsP2H3bcqX3TNaEomhjxk7HVAGpQe0gXUGlNrZZATGhGJiQyE5R8O8BOm', '2018-02-10 00:00:07', '2018-02-10 00:00:07'),
(7, 'Deepak Raj Giri', 'deepak@robotics.wrc.edu.np', NULL, '$2y$10$.JGA1f5HTBlRIzER3Kxd1uUoAeejF4JWlRCj5EV7ahr5xy29Zkccu', 'nDzPg1TOv6VvUQWLzd7urWcSNHTCEUDvz146fg7K02Tvzk3ODKiXDMVIpxdQ', '2018-05-12 12:27:45', '2018-05-12 12:27:45'),
(8, 'DHOD', 'dhod@robotics.wrc.edu.np', NULL, '$2y$10$xF510CAoKapTg638pmY0zeHFfBWEOzy6fLGNLQqmB5HyNX/Q7gY2G', 'e5uduqlMgPwsl9Zyjy2itlxdjOH6wydE12Dlor3NtY66xbZMFenJeRtO46Ep', '2018-05-12 12:29:35', '2018-05-12 12:29:35');

-- --------------------------------------------------------

--
-- Table structure for table `users_tasks`
--

CREATE TABLE `users_tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `tasks_id` int(10) UNSIGNED NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_tasks`
--

INSERT INTO `users_tasks` (`id`, `user_id`, `tasks_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 'finished', '2018-02-10 00:09:00', '2018-02-10 00:00:00'),
(2, 2, 2, 'running', '2018-02-10 00:00:00', '2018-02-10 00:00:00'),
(3, 3, 2, 'started', '2018-02-10 00:00:00', '2018-02-10 00:00:00'),
(4, 4, 1, 'running', '2018-02-10 00:00:00', '2018-02-10 00:00:00'),
(5, 5, 3, 'running', '2018-02-10 00:00:00', '2018-02-10 00:00:06'),
(6, 1, 5, 'running', '2018-02-10 00:00:00', '2018-02-10 00:00:06'),
(7, 1, 6, 'finished', '2018-02-10 00:00:00', '2018-02-10 00:00:06');

-- --------------------------------------------------------

--
-- Table structure for table `user_projects`
--

CREATE TABLE `user_projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `projects_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_projects`
--

INSERT INTO `user_projects` (`id`, `user_id`, `projects_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 2, '2018-02-10 08:51:38', '2018-02-10 08:51:38'),
(2, 5, 1, 2, '2018-02-10 09:00:44', '2018-02-10 09:00:44'),
(3, 3, 1, 2, '2018-02-10 09:09:09', '2018-02-10 09:09:09'),
(4, 2, 1, 2, '2018-02-10 09:09:50', '2018-02-10 09:09:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_user`
--
ALTER TABLE `group_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `materials_material_category_id_foreign` (`material_category_id`);

--
-- Indexes for table `material_category`
--
ALTER TABLE `material_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material_issue`
--
ALTER TABLE `material_issue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `material_issue_materials_id_foreign` (`materials_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `phases`
--
ALTER TABLE `phases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phases_projects_id_foreign` (`projects_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projects_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_phases_id_foreign` (`phases_id`);

--
-- Indexes for table `task_report`
--
ALTER TABLE `task_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_report_users_tasks_id_foreign` (`users_tasks_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_tasks`
--
ALTER TABLE `users_tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_tasks_user_id_foreign` (`user_id`),
  ADD KEY `users_tasks_tasks_id_foreign` (`tasks_id`);

--
-- Indexes for table `user_projects`
--
ALTER TABLE `user_projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_projects_user_id_foreign` (`user_id`),
  ADD KEY `user_projects_projects_id_foreign` (`projects_id`),
  ADD KEY `user_projects_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `group_user`
--
ALTER TABLE `group_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `materials`
--
ALTER TABLE `materials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `material_category`
--
ALTER TABLE `material_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `material_issue`
--
ALTER TABLE `material_issue`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `phases`
--
ALTER TABLE `phases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `task_report`
--
ALTER TABLE `task_report`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users_tasks`
--
ALTER TABLE `users_tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user_projects`
--
ALTER TABLE `user_projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `materials`
--
ALTER TABLE `materials`
  ADD CONSTRAINT `materials_material_category_id_foreign` FOREIGN KEY (`material_category_id`) REFERENCES `material_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `material_issue`
--
ALTER TABLE `material_issue`
  ADD CONSTRAINT `material_issue_materials_id_foreign` FOREIGN KEY (`materials_id`) REFERENCES `materials` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `phases`
--
ALTER TABLE `phases`
  ADD CONSTRAINT `phases_projects_id_foreign` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_phases_id_foreign` FOREIGN KEY (`phases_id`) REFERENCES `phases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `task_report`
--
ALTER TABLE `task_report`
  ADD CONSTRAINT `task_report_users_tasks_id_foreign` FOREIGN KEY (`users_tasks_id`) REFERENCES `users_tasks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_tasks`
--
ALTER TABLE `users_tasks`
  ADD CONSTRAINT `users_tasks_tasks_id_foreign` FOREIGN KEY (`tasks_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_tasks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_projects`
--
ALTER TABLE `user_projects`
  ADD CONSTRAINT `user_projects_projects_id_foreign` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_projects_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_projects_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
