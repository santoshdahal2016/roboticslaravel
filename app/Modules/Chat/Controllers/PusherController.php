<?php
/**
 * Created by PhpStorm.
 * User: santosh
 * Date: 2/20/18
 * Time: 6:33 PM
 */

namespace App\Modules\Chat\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Config;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Pusher\Pusher;

class PusherController extends Controller {

    //accessed through '/pusher/'
    //setup your routes.php accordingly

    public function __construct() {
        $this->middleware('auth');
//        parent::__construct();
        //Let's register our pusher application with the server.
        //I have used my own config files. The config keys are self-explanatory.
        //You have received these config values from pusher itself, when you signed up for their service.
    }

    /**
     * Authenticates logged-in user in the Pusher JS app
     * For presence channels
     */
    public function postAuth()
    {
//        $user = Auth::user();
//        dd($user);
        //We see if the user is logged in our laravel application.
        if(Auth::check())
        {
            //Fetch User Object
            $user =  Auth::user();
            //Presence Channel information. Usually contains personal user information.
            //See: https://pusher.com/docs/client_api_guide/client_presence_channels
            $presence_data = array('name' => $user->name);
            //Registers users' presence channel.
            echo $this->pusher->presence_auth(Input::get('channel_name'), Input::get('socket_id'), $user->id, $presence_data);
        }
        else
        {
            return '403';
        }
    }
}