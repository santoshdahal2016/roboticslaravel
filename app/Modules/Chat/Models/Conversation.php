<?php

namespace App\Modules\Chat\Models;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $fillable = ['message', 'user_id', 'group_id'];
    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }
    public function group()
    {
        return $this->belongsTo('App\Modules\Chat\Models\Group');
    }
}
