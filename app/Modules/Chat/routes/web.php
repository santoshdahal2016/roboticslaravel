<?php

Route::group(['module' => 'Chat', 'middleware' => ['web'], 'namespace' => 'App\Modules\Chat\Controllers'], function() {

    Route::resource('Chat', 'ChatController');
    Route::resource('groups', 'GroupController');
    Route::resource('conversations', 'ConversationController');
    Route::post('/pusher/auth','PusherController@postAuth');

});
