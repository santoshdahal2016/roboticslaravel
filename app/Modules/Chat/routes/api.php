<?php

Route::group(['module' => 'Chat', 'middleware' => ['api'], 'namespace' => 'App\Modules\Chat\Controllers'], function() {

    Route::resource('Chat', 'ChatController');

});
