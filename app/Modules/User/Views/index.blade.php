@extends( 'User::app' )
@section('content-header')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">User</h4></div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i
                        class="ti-settings text-white"></i></button>

            <ol class="breadcrumb">
                <li><a href="#">Users</a></li>
                <li class="active">Users</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection
@section('styles')
    <script src="https://unpkg.com/vue@2.5.16/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
    <script type="text/javascript" src="//unpkg.com/uiv/dist/uiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment-with-locales.min.js"></script>
@endsection
@section('content')
    <div id="user">

        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title">Manage Users</h3>
                    <h3 class="box-title"><a href="{{ url('/user_create') }}">
                            <button class="btn btn-sm btn-success">Add User</button>
                        </a></h3>

                    <div class="table-responsive manage-table">
                        <table class="table" cellspacing="14">
                            <thead>
                            <tr>

                                <th >Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Year</th>
                                <th>Faculty</th>
                                <th>Project</th>

                                <th>Collaboration</th>

                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>


                            <tr v-for="(item ,i) in users " class="advance-table-row ">

                                <td>@{{item.name}}</td>
                                <td>@{{item.email}}</td>
                                <td>
                                    <span v-for="(item1 ,i) in item.role "> @{{ item1.display_name }}</span>
                                </td>
                                <td>@{{item.year}}</td>
                                <td>@{{item.faculty}}</td>
                                <td>
                                    <span v-for="(item2 ,i) in item.projects "> @{{ item2.name }} <br></span>
                                </td>
                                <td>
                                    <span v-for="(item3 ,i) in item.collaboration "> @{{ item3.name }} <br></span>
                                </td>

                                <td>                                    <a class="btn btn-default" @click="redit(item.id)">Edit</a>
                                    <a v-if="item.active == 0" class="btn btn-success" @click="activate(item.id)">Active</a>
                                    {{--<a href="{{ url('/user_delete/'.$user->id) }}">--}}
                                        {{--<button class="btn btn-danger">Delete</button>--}}
                                    {{--</a>--}}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" class="sm-pd"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection


@section('scripts')



    <script type="text/javascript">


        var rr = new Vue({
            el: '#user'
            ,
            components: {},
            data: function () {
                return {
                    users: {!! $users !!},
                    open: false,
                    edit: "/user_edit/"

                }
            },
            computed: {},

            methods: {

                redit(id) {
                    window.location.href = this.edit + id;

                },

                activate(id) {
                    axios.post('/user/active', {
                        'id': id,
                    })
                        .then(res => {
                            // console.log(res.data)
                            this.users = res.data.users;


                            this.$notify({
                                title: 'Activated',
                                content:"Finish",
                            });
                        })
                        .catch(err => {
                            console.log(err);
                            this.$notify({
                                type: 'warning',
                                title: 'Warning!',
                                content: 'Please check your input and try again Or Refresh Once.'
                            })
                        })
                }

            }
        });
    </script>
@endsection



