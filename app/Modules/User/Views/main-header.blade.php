<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">
        <div class="top-left-part">
            <!-- Logo -->
            <a class="logo" href="">
                <!-- Logo icon image, you can use font-icon also --><b>
                    <!--This is dark logo icon--><img width="190px" height="55px" src="http://www.roboticsconference.org/images/rsslogotext-white.png" alt="home"
                                                      class="dark-logo"/><!--This is light logo icon-->
                </b>
                {{--<!-- Logo text image you can use text also --><span class="hidden-xs">--}}
                        {{--<!--This is dark logo text--><img src="https://i.stack.imgur.com/aNryi.png" alt="home"--}}
                                                          {{--class="dark-logo"/>--}}
                     {{--</span> </a>--}}
        </div>
        <!-- /Logo -->
        <!-- Search input and Toggle icon -->
        <ul class="nav navbar-top-links navbar-left">
            <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs"><i
                            class="ti-close ti-menu"></i></a></li>

        </ul>
        <ul class="nav navbar-top-links navbar-right pull-right">
            <li>
                <form role="search" class="app-search hidden-sm hidden-xs m-r-10">
                    <input type="text" placeholder="Search..." class="form-control"> <a href=""><i
                                class="fa fa-search"></i></a></form>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img
                            src="http://image.dongascience.com/Photo/2014/06/14028776223518.png" alt="user-img" width="36" class="img-circle"><b
                            class="hidden-xs">{{Auth::user()->name}}</b><span class="caret"></span> </a>
                <ul class="dropdown-menu dropdown-user animated flipInY">
                    <li>
                        <div class="dw-user-box">
                            <div class="u-img"><img src="http://image.dongascience.com/Photo/2014/06/14028776223518.png" alt="user"/></div>
                            <div class="u-text"><h4>{{Auth::user()->name}}</h4>
                                <p class="text-muted"> {{{ isset(Auth::user()->name) ? Auth::user()->email : Auth::user()->email }}}</p></div>
                        </div>
                    </li>

                    <li><a href="{{ url('/account/setting') }}"><i class="ti-settings"></i> Account Setting</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ url('/logout') }}"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
                <!-- /.dropdown-user -->
            </li>

            <!-- /.dropdown -->
        </ul>
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
</nav>



