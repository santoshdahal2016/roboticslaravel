@extends( 'User::app' )
@section('content-header')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">User</h4></div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i
                        class="ti-settings text-white"></i></button>

            <ol class="breadcrumb">
                <li><a href="#">Users</a></li>
                <li class=""><a href="{{url('/user')}}">Users</a></li>
                <li class="active">Register</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection
@section('content')
    <div class="row">
        @include('User::errors')

        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading"> New User</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form action="{{ url('/user_create/') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-body">
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Name</label>
                                            <input type="text" name="name" id="firstName" class="form-control"
                                                   placeholder="John doe"></div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <label class="control-label">Email</label>
                                            <input type="email" name="email" id="lastName" class="form-control"
                                                   placeholder="dummy@gmail.com"></div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <label class="control-label">Password</label>
                                            <input type="password" required='required' class="form-control"
                                                   name="password" id="inputPassword3"
                                                   placeholder="Password">                                        </div>

                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Confirm Password</label>
                                            <input type="password" required='required' class="form-control"
                                                   name="password_confirmation" id="inputPasswordC3"
                                                   placeholder="Confirm Password">
                                        </div></div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Role:</label>

                                                    {!! Form::select('roles_id',$all_roles, 'Choose Role', ['class' => 'form-control','required'=>'required']) !!}

                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">

                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->

                                <!--/row-->

                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
                                <button type="button" class="btn btn-default">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection