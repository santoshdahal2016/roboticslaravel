<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home |Robotics Club</title>

    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    {{--<link href="{{ url('/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">--}}

    {{--<!-- Menu CSS -->--}}
    {{--<link href="{{url('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css')}}" rel="stylesheet">--}}
    {{--<link href="{{url('plugins/bower_components/typeahead.js-master/dist/typehead-min.css')}}" rel="stylesheet">--}}

    {{--<!-- Vector CSS -->--}}
    {{--<link href="{{url(' plugins/bower_components/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet"/>--}}
    {{--<!-- animation CSS -->--}}
    {{--<link href="{{url('css/animate.css') }}" rel="stylesheet">--}}
    {{--<!-- Custom CSS -->--}}
    {{--<link href="{{url('css/style.css ') }}" rel="stylesheet">--}}
    {{--<!-- color CSS -->--}}
    {{--<link href="{{url('css/colors/default.css') }}" id="theme" rel="stylesheet">--}}
    {{--<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->--}}
    {{--<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->--}}
    {{--<!--[if lt IE 9]>--}}
    {{--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>--}}
    {{--<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>--}}
    {{--<![endif]-->--}}


    @yield('styles')

</head>
<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">


    @include('User::main-header')
    @include('User::main-sidebar')

    <div id="page-wrapper">
        <div class="container-fluid" id="app">
            @yield('content-header')
            @yield('content')
            <footer class="footer text-center"> @php echo date('Y') @endphp &copy; Robotics Club</footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
</div>


<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ asset('assets/dist/js/app.min.js') }}"></script>


<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
{{--<script src="{{ url('plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>--}}
{{--<!-- Bootstrap Core JavaScript -->--}}
{{--<script src="{{ url('bootstrap/dist/js/bootstrap.js') }}"></script>--}}
{{--<!-- Menu Plugin JavaScript -->--}}
{{--<script src="{{ url('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>--}}
{{--<!--slimscroll JavaScript -->--}}
{{--<script src="{{ url('js/jquery.slimscroll.js') }}"></script>--}}
{{--<!--Wave Effects -->--}}
{{--<script src="{{ url('js/waves.js') }}"></script>--}}
{{--<!--Counter js -->--}}
{{--<!-- Custom Theme JavaScript -->--}}
{{--<script src="{{ url('js/custom.min.js') }}"></script>--}}
{{--<script src="{{ url('plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js') }}"></script>--}}
{{--<script src="{{ url('plugins/bower_components/typeahead.js-master/dist/typeahead-init.js') }}"></script>--}}
<!-- Custom tab JavaScript -->
{{--<script src="{{ url('js/cbpFWTabs.js') }}"></script>--}}
{{--<script type="text/javascript">--}}
{{--(function () {--}}
{{--[].slice.call(document.querySelectorAll('.sttabs')).forEach(function (el) {--}}
{{--new CBPFWTabs(el);--}}
{{--});--}}
{{--})();--}}
{{--</script>--}}
{{--<script src="{{ url('plugins/bower_components/toast-master/js/jquery.toast.js') }}"></script>--}}
<!--Style Switcher -->

{{--<script src="{{ url('plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>--}}
@yield('scripts')

</body>

</html>

