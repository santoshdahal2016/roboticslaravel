@extends( 'User::app' )
@section('content-header')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">User</h4></div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i
                        class="ti-settings text-white"></i></button>

            <ol class="breadcrumb">
                <li><a href="#">Users</a></li>
                <li class="active">Roles</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title">Manage Roles</h3>
                <h3 class="box-title"><a href="{{ url('/role_create') }}">
                        <button class="btn btn-sm btn-success">Add Roles</button></a></h3>

                <div class="table-responsive manage-table">
                    <table class="table" cellspacing="14">
                        <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Name</th>
                            <th>Display Name</th>
                            <th>Description</th>
                            <th>Permissions</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($role as $item)
                            <tr class="advance-table-row ">
                                <td width="10"></td>
                                <td width="10"></td>

                                <td width="40">
                                    <div class="checkbox checkbox-circle checkbox-info">
                                        <input id="checkbox7"  type="checkbox">
                                        <label for="checkbox7"> </label>
                                    </div>
                                </td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->display_name }}</td>
                                <td>{{ $item->description}}</td>
                                <td>
                                    <ul>
                                        <?php $permissions = $item->perms()->get(); ?>
                                        @if(!empty($permissions))
                                            @foreach($permissions as $role_perm)
                                                <li>{{ $role_perm->display_name }}</li>
                                            @endforeach
                                        @else
                                            {{ '-' }}
                                        @endif
                                    </ul>
                                </td>
                                <td><a href="{{ url('/role_edit/'.$item->id) }}">
                                        <button class="btn btn-primary">Edit</button>
                                    </a> <a href="{{ url('/role_delete/'.$item->id) }}">
                                        <button class="btn btn-danger">Delete</button>
                                    </a></td>
                            </tr>
                            <tr>
                                <td colspan="7" class="sm-pd"></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')

@endsection


