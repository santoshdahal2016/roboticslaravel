@extends( 'User::app' )
@section('styles')
@endsection
@section('content-header')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Dashboard </h4></div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

            <ol class="breadcrumb">
                <li><a href="#">Dashboard</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
@endsection

@section('content')
    <div class="row">
        @foreach($projects as $item)

        <div class="col-md-6 col-lg-3 col-xs-12 col-sm-6"> <a href="/project/{{$item->id}}">   <img class="img-responsive" alt="user" src="/images/project.jpg">
           <div class="white-box">
               <div class="text-muted"><span class="m-r-10"><i class="icon-calender"></i> {{ $item->created_at->diffForHumans() }}</span> <a class="text-muted m-l-10" href="javascript:void(0)"><i class="fa fa-heart-o"></i> 38</a></div>
                <h3 class="m-t-20 m-b-20">{{$item->name}}</h3>
                <p>{!! $item->description !!}</p>

            </div>
            </a>
        </div>

       @endforeach

    </div>
@endsection


@section('scripts')

@endsection

