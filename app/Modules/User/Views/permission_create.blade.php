@extends( 'User::app' )
@section('content-header')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Permission</h4></div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i
                        class="ti-settings text-white"></i></button>

            <ol class="breadcrumb">
                <li><a href="{{ url('/dashboard') }}"> Home</a></li>
                <li class="active">Permission</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection
@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/select2.min.css') }}">
@endsection

@section('content')
    <div class="row">


        @include('User::errors')
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading"> New Permission</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">

                        <form action="{{ url('/permission_create/') }}" method="post">
                            {{ csrf_field() }}
                            <div class="box-body">

                                <label for="inputName3">Name</label>

                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" required='required' class="form-control" name="name"
                                               id="inputName3" placeholder="Name" value="{{ old('name') }}">
                                        <div class="help-info">*Required</div>
                                    </div>
                                </div>

                                <label for="inputEmail3">Display name</label>

                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" required='required' class="form-control" name="display_name"
                                               id="inputName3" placeholder="Display Name" value="{{ old('name') }}">
                                        <div class="help-info">*Required</div>
                                    </div>
                                </div>

                                <label for="inputPassword3">Description</label>

                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" required='required' class="form-control" name="description"
                                               id="inputName3" placeholder="Description" value="{{ old('name') }}">
                                        <div class="help-info">*Required</div>
                                    </div>
                                </div>


                                <label for="roles_list">Attach Role/s</label>

                                <div class="form-group">
                                    <div class="form-line">
                                        {!! Form::select('roles_list[]',$drop_roles, null, ['class' => 'ms','style'=>'width:100%', 'multiple'=>'multiple' , 'id'=>'optgroup']) !!}

                                    </div>
                                </div>
                            </div> <!-- /.box-body -->

                            <button type="submit" class="btn btn-info ">Add Permission</button>

                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
                <!-- /.col-md-6 -->
            </div>
        </div>
    </div>
    <!-- /.row -->
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/select2/select2.full.min.js') }}"></script>
    <script type="text/javascript">
        $("#optgroup").select2({
            placeholder: 'Choose Permission'
        });

    </script>

@endsection