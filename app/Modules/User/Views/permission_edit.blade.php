@extends( 'User::app' )
@section('content-header')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Permission</h4></div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i
                        class="ti-settings text-white"></i></button>

            <ol class="breadcrumb">
                <li><a href="{{ url('/dashboard') }}"> Home</a></li>
                <li class="active">Permission</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection
@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/select2.min.css') }}">
@endsection

@section('content')
    <div class="row">


        @include('User::errors')
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading"> New Permission</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">


                    {!! Form::model($permission, ['method' => 'post','url' => '/permission_edit/'.$permission->id,'class'=>'']) !!}
            {{ csrf_field() }}
            <div class="box-body">
               
                    <label for="inputName3" >Name</label>

                   <div class="form-group">
                            <div class="form-line">
                        {!! Form::text('name', null, ['placeholder'=>'name','class'=>'form-control','required'=>'required']) !!}
                                <div class="help-info">*Required</div></div>
                </div>
               
                    <label for="inputEmail3" >Display Name</label>

                   <div class="form-group">
                            <div class="form-line">
                        {!! Form::text('display_name', null, ['placeholder'=>'Display Name','class'=>'form-control','required'=>'required']) !!}
                                <div class="help-info">*Required</div></div>
                </div>
               
                    <label for="inputEmail3" >Description</label>

                   <div class="form-group">
                            <div class="form-line">
                        {!! Form::text('description', null, ['placeholder'=>'Description','class'=>'form-control','required'=>'required']) !!}
                                <div class="help-info">*Required</div></div>
                </div>

               
                    <label for="roles_list" >Attach Role/s</label>

                   <div class="form-group">
                            <div class="form-line">
                        {!! Form::select('roles_list[]',$drop_roles, $select_roles, ['id' => 'optgroup','style'=>'width:100%','class' => 'ms', 'multiple','required'=>'required']) !!}
                                <div class="help-info">*Required</div> </div>
                </div>
            </div>
            <!-- /.box-body -->
           
                <button type="submit" class="btn btn-info ">Update Permission</button>
          
            <!-- /.box-footer -->
            {!! Form::close() !!}
        </div>
    </div>
    <!-- /.col-md-6 -->
</div>
<!-- /.row -->
@endsection

@section('scripts')
<script  src="{{ asset('assets/plugins/select2/select2.full.min.js') }}"></script>
    <script type="text/javascript">
        $("#optgroup").select2({
            placeholder: 'Choose Roles'
        });

    </script>

@endsection