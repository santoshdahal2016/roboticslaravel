<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <?php $menus = get_active_menu_items(); ?>
            <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i
                            class="ti-close visible-xs"></i></span> <span class="hide-menu">Navigation</span></h3></div>
        <ul class="nav" id="side-menu">
            <li class="user-pro">
                <a href="#" class="waves-effect"><img src="http://image.dongascience.com/Photo/2014/06/14028776223518.png" alt="user-img"
                                                      class="img-circle"> <span class="hide-menu">
                        {{{ isset(Auth::user()->name) ? Auth::user()->email : Auth::user()->email }}}<span
                                class="fa arrow"></span></span>
                </a>
                <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">

                    <li><a href="{{ url('/account/setting') }}"><i class="ti-settings"></i> <span
                                    class="hide-menu">Account Setting</span></a></li>
                    <li><a href="{{ url('/logout') }}"><i class="fa fa-power-off"></i> <span
                                    class="hide-menu">Logout</span></a></li>
                </ul>
            </li>

            <li><a href="#" class="waves-effect {{ (($menus['main_menu'] == 1)?'active' :'') }}"><i class="mdi mdi-av-timer fa-fw" data-icon="v"></i>
                    <span class="hide-menu"> Dashboard <span class="fa arrow"></span> <span
                                class="label label-rouded label-inverse pull-right">1</span></span></a>
                <ul class="nav nav-second-level">

                    <li class="">
                        <a href="{{ url('/dashboard') }}" class="waves-effect {{ (($menus['sub_menu'] == 10)?'active' :'') }}"> <i
                                    class=" ti-eye "></i> <span class="hide-menu">Dashboard</span></a>
                    </li>


                </ul>
            </li>

            @ability('developer','user_index')
            <li>
                <a href="#" class="{{ (($menus['main_menu'] == 2)?'active' :'') }} "><i class="ti-user fa-fw" data-icon="v"></i>
                    <span class="hide-menu"> Users <span class="fa arrow"></span> <span
                                class="label label-rouded label-inverse pull-right">3</span></span>
                </a>
                <ul class="nav nav-second-level">

                    @ability('developer', 'user_index')
                    <li class="">
                        <a href="{{ url('/user') }}"
                           class="waves-effect {{ (($menus['sub_menu'] == 21)?'active' :'') }}"> <i
                                    class="icon-user-follow fa-fw"></i> <span class="hide-menu">Users</span></a>
                    </li>
                    @endability
                    @ability('developer', 'user_role_index')
                    <li class="">
                        <a href="{{ url('/role') }}"
                           class="waves-effect {{ (($menus['sub_menu'] == 22)?'active' :'') }}"> <i
                                    class=" ti-unlink "></i> <span class="hide-menu">Roles</span></a>
                    </li>
                    @endability
                    @ability('developer', 'user_permission_index')
                    <li class="">
                        <a href="{{ url('/permission') }}"
                           class=" {{ (($menus['sub_menu'] == 23)?'active' :'') }}"><i
                                    class="ti-key "></i> <span class="hide-menu">Premissions</span></a>
                    </li>
                    @endability

                </ul>
            </li>
            @endability
            @ability('developer','project')
            <li><a href="#" class="waves-effect {{ (($menus['main_menu'] == 3)?'active' :'') }}"><i class="ti-rocket fa-fw" data-icon="v"></i>
                    <span class="hide-menu"> Project <span class="fa arrow"></span> <span
                                class="label label-rouded label-inverse pull-right">1</span></span></a>
                <ul class="nav nav-second-level">

                    <li class="">
                        <a href="{{ url('/project') }}" class="waves-effect {{ (($menus['sub_menu'] == 30)?'active' :'') }}"> <i
                                    class="ti-desktop fa-fw"></i> <span class="hide-menu">Projects</span></a>
                    </li>


                </ul>
            </li>
            @endability

            @ability('developer','material_index')
            <li><a href="#" class="waves-effect {{ (($menus['main_menu'] == 4)?'active' :'') }}"><i class="ti-server fa-fw" data-icon="v"></i>
                    <span class="hide-menu"> Material <span class="fa arrow"></span> <span
                                class="label label-rouded label-inverse pull-right">1</span></span></a>
                <ul class="nav nav-second-level">

                    <li class="">
                        <a href="{{ url('/material') }}" class="waves-effect {{ (($menus['sub_menu'] == 40)?'active' :'') }}"> <i
                                    class="ti-desktop fa-fw"></i> <span class="hide-menu">Material</span></a>
                    </li>

                </ul>
            </li>
            @endability


        </ul>
    </div>
</div>
