<?php

namespace App\Modules\User\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
//use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
//use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;


class User extends Model implements AuthenticatableContract,
    CanResetPasswordContract
{
    use EntrustUserTrait, Authenticatable, CanResetPassword, Notifiable;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password','phone','year','faculty','phone','sessions_id','active'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    public function role()
    {
        return $this->belongsToMany('App\Modules\User\Models\Role');

    }

    public function projects()
    {
        return $this->hasMany('App\Modules\Project\Models\Project', 'user_id');
    }


    public function collaboration()
    {
        return $this->belongsToMany('App\Modules\Project\Models\Project', 'user_projects','user_id','projects_id');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Modules\Chat\Models\Group')->withTimestamps();
    }

}
