<?php namespace App\Modules\User\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Email\Models\Email_credentials;
use App\Modules\Event\Models\Event;
use App\Modules\Project\Models\Project;
use App\Modules\Project\Models\UserProject;
use App\Modules\User\Models\User;
use App\Modules\Question\Models\Question;
use Illuminate\Support\Facades\Log;
use App\Modules\Lottery\Models\Lottery;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Zizaco\Entrust\Entrust;
use App\Modules\User\Models\Permission;
use App\Modules\User\Models\Role;
use App\Modules\Sms\Models\Sms_credentials;

/**
 * Class UserController
 * @package App\Modules\User\Controllers
 */
class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['home']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index()
    {
        $user = Auth::user();

        if ($user->hasRole('developer') || $user->hasRole('cithead') || $user->hasRole('subhod')) {
            $users = User::with('projects' ,'collaboration','role')->latest('created_at')->get();
//            dd($users);
            return view('User::index')->with('users', $users);

        } elseif ($user->can('user_index')) {
            $users = User::with('projects' ,'collaboration','role')->latest('created_at')->get();
            return view('User::index')->with('users', $users);
        } else {
            abort(403);
        }

    }

    public function active(Request $request){

        $user = User::findOrFail($request->id);
        $user->active = 1 ;
        $user->save();
        $users = User::with('projects' ,'collaboration','role')->latest('created_at')->get();
        $data['users'] = $users;
        return json_encode($data);

    }

    public function dashboard()
    {
        $user = Auth::user();
        $collaboration = UserProject::WhereIn('role_id', ['2'])->Where('user_id', $user->id)->get()->pluck('projects_id')->toArray();
        $projects = Project::where('user_id', $user->id)->orWhereIn('id', $collaboration)->get();

        return view('User::dashboard')->with(['projects' => $projects]);


    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function report(Request $request)
    {
//        dd($request->all());
        $report = $request->report;
        $original = Auth::user();
        $original['report'] = $report;
        $original['updated_at'] = Carbon::now()->toDateTimeString();;
        $original->save();
        return redirect()->back();


    }

    public function home()
    {
        return view('User::home');


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user = Auth::user();
        $all_roles = Role::pluck('display_name', 'id');
        if ($user->hasRole('developer') || $user->can('user_create')) {
            return view('User::register')->with('all_roles', $all_roles);
        } else {
            abort(403);
        }

    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        if ($user->hasRole('developer') || $user->can('user_store')) {
            $this->validate($request, [
                'name' => 'required|min:3',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6|confirmed',
                'roles_id' => 'required'
            ]);
            $user_input = $request->all();
            $user_input['password'] = bcrypt($user_input['password']);

            $created_user = User::create($user_input);
            $created_user->roles()->attach($request['roles_id']);

            return redirect('/user/');
            // }
        } else {
            abort(403);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $user = Auth::user();

        $all_roles = Role::pluck('display_name', 'id');
        if ($user->hasRole('developer') || $user->hasRole('cithead') || $user->hasRole('subhod')) {

            $this_user = User::findOrFail($id);
            $selected_role = $this_user->roles()->first()->id;
            return view('User::edit')->with(['user' => $this_user, 'all_roles' => $all_roles, 'selected_role' => $selected_role]);
        } elseif ($user->can('user_edit') && $user->id == $id) {
            $this_user = User::findOrFail($id);
            $selected_role = $this_user->roles()->first()->id;
            return view('User::edit')->with(['user' => $this_user, 'all_roles' => $all_roles, 'selected_role' => $selected_role]);
        } else {
            abort(403);
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        if ($user->hasRole('developer') || $user->hasRole('cithead') || $user->hasRole('subhod')) {

            $user_update = User::findOrFail($id);

            $this->validate($request, [
                'name' => 'required|min:3',
                'email' => 'required|email',
                'password' => 'required|confirmed|min:6',
                'roles_id' => 'required'
            ]);

            $user_input = $request->all();
            $user_input['password'] = bcrypt($user_input['password']);
            $user_update->fill($user_input)->save();

            $user_update->roles()->sync($request['roles_id']);
            return redirect('/user/');
        } elseif ($user->can('user_update') && $user->id == $id) {
            $user_update = User::findOrFail($id);

            $this->validate($request, [
                'name' => 'required|min:3',
                'email' => 'required|email',
                'password' => 'required|confirmed|min:6',
                'roles_id' => 'required'
            ]);

            $user_input = $request->all();
            $user_input['password'] = bcrypt($user_input['password']);
            $user_update->fill($user_input)->save();

            $user_update->roles()->sync($request['roles_id']);
            return redirect('/user/');
        } else {
            abort(403);
        }
    }


    public function setting()
    {
        $user = Auth::user();
        return view('User::user.setting')->with(['user' => $user]);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function setting_store(Request $request)
    {
        $user = Auth::user();


        $this->validate($request, [
            'name' => 'required|min:3',
            'password' => 'required|confirmed|min:6',
        ]);

        $user_input = $request->all();
        $user_input['password'] = bcrypt($user_input['password']);
        $user->fill($user_input)->save();

        return redirect('/logout/');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $user_delete = User::findOrFail($id);
        if ($user->hasRole('developer') || $user->hasRole('cithead') || $user->hasRole('subhod')) {
            $user_delete->delete();
            return redirect('/user/');
        } elseif ($user->can('user_destroy') && $user->id == $user_delete->id) {
            $user_delete->delete();
            return redirect('/user/');
        } else {
            abort(403);
        }
    }
}
