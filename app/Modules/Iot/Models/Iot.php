<?php

namespace App\Modules\Iot\Models;

use Illuminate\Database\Eloquent\Model;

class Iot extends Model {

    protected  $table = "iot";

    protected $fillable = ['temperature' ,'humidity'];


}
