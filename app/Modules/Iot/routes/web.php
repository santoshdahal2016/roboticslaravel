<?php

Route::group(['module' => 'Iot', 'middleware' => ['web'], 'namespace' => 'App\Modules\Iot\Controllers'], function() {

    Route::get('/upload','IotController@upload');
    Route::get('/iot/api','IotController@api');


});
