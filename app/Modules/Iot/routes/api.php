<?php

Route::group(['module' => 'Iot', 'middleware' => ['api'], 'namespace' => 'App\Modules\Iot\Controllers'], function() {

    Route::resource('Iot', 'IotController');

});
