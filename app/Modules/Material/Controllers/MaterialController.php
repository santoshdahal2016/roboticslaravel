<?php

namespace App\Modules\Material\Controllers;

use App\Modules\Material\Models\Material;
use App\Modules\Material\Models\Materialcategory;
use App\Modules\Material\Models\Materialissue;
use App\Modules\Project\Models\Project;
use App\Modules\User\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use MaterialStatus;
use Validator;

class MaterialController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole('developer') || $user->can('material_index')) {

            $material = Material::with('category', 'issued')->withCount('issued')->get();
            $users = User::all();
            $projects = Project::all()->count();
            return view("Material::index")->with(['materials' => $material, "users" => $users, "projects" => $projects]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user->hasRole('developer') || $user->can('material_create')) {

            $category = Materialcategory::all();
            return view("Material::create")->with(["category" => $category]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if ($user->hasRole('developer') || $user->can('material_create')) {

            $this->validate($request, [
                'name' => 'required|min:3',
                'description' => 'required',
                'material_category_id' => 'required',
                'quantity' => 'required|numeric',
                'source' => 'required|numeric',


            ]);
            $material['quantity'] = $request['quantity'];
            $material['material_category_id'] = $request['material_category_id'];
            $material['name'] = $request['name'];
            $material['description'] = $request['description'];
            $material['source'] = $request['source'];

            Material::create($material);

            $request->session()->flash('success', $material['name'] . ' created');

            return redirect('/material/');
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store_category(Request $request)
    {
        $user = Auth::user();
        if ($user->hasRole('developer') || $user->can('material_create')) {

//        dd($request->all());
            $materialcategory = Materialcategory::create([
                    'name' => request('name'),
                    'description' => request('description')
                ]
            );
            return $materialcategory;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        if ($user->hasRole('developer') || $user->can('material_update')) {

            $material = Material::findOrFail($id);
            $category = Materialcategory::pluck('name', 'id')->all();
            return view("Material::edit")->with(["category" => $category, "material" => $material]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        if ($user->hasRole('developer') || $user->can('material_update')) {

            $this->validate($request, [
                'name' => 'required|min:3',
                'description' => 'required',
                'material_category_id' => 'required',
                'quantity' => 'required|numeric',
                'source' => 'required|numeric',


            ]);

            $material = Material::findorFail($id);
            $material->fill($request->all())->save();
            $request->session()->flash('success', $material['name'] . ' Updated');

            return redirect('/material/');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function issue(Request $request)
    {
        $user = Auth::user();
        if ($user->hasRole('developer') || $user->can('material_issue')) {

            $validator = Validator::make($request->all(), [
                'user_email' => 'required|email|exists:users,email',
                'material_id' => 'required|numeric|exists:materials,id',
                'deadline' => 'required|date'

            ]);

            if ($validator->fails()) {
                $return = ["status" => "error",
                    "error" => [
                        "code" => 422,
                        "errors" => $validator->errors()
                    ]];

                return response()->json($return, 422);

            }

            $material = Material::with('category', 'issued')->withCount('issued')->where('id', $request->material_id)->first();

            if ($material->quantity > $material->issued_count) {
                $user = User::where('email', $request->user_email)->first();
                $materialissue['user_id'] = $user->id;
                $materialissue['materials_id'] = $request->material_id;
                $materialissue['deadline'] = $request->deadline;

                Materialissue::create($materialissue);
            } else {
                $return = ["status" => "error",
                    "message" => "Over Exceed"];

                return response()->json($return, 422);
            }

            $materials = Material::with('category', 'issued')->withCount('issued')->get();
            $users = User::all();

            $data['materials'] = $materials;
            $data['users'] = $users;

            return json_encode($data);
        }

    }

    public function return(Request $request)
    {
        $user = Auth::user();
        if ($user->hasRole('developer') || $user->can('material_issue')) {

            $validator = Validator::make($request->all(), [
                'id' => 'required|numeric|exists:material_issue,id',

            ]);

            if ($validator->fails()) {
                $return = ["status" => "error",
                    "error" => [
                        "code" => 422,
                        "errors" => $validator->errors()
                    ]];

                return response()->json($return, 422);

            }

            $materialissue = Materialissue::findOrFail($request->id);
            $materialissue->status = MaterialStatus::return;
            $materialissue->save();

            $materials = Material::with('category', 'issued')->withCount('issued')->get();
            $users = User::all();

            $data['materials'] = $materials;
            $data['users'] = $users;

            return json_encode($data);
        }

    }

    public function reissue(Request $request)
    {

        $user = Auth::user();
        if ($user->hasRole('developer') || $user->can('material_issue')) {

            $validator = Validator::make($request->all(), [
                'id' => 'required|numeric|exists:material_issue,id',
                'email' => 'required|email|exists:users,email'

            ]);

            if ($validator->fails()) {
                $return = ["status" => "error",
                    "error" => [
                        "code" => 422,
                        "errors" => $validator->errors()
                    ]];

                return response()->json($return, 422);

            }

            $materialissue = Materialissue::findOrFail($request->id);

            if ($request->email != $materialissue->user->email) {
                $return = ["status" => "error",
                    "error" => "Email not matched"];

                return response()->json($return, 422);

            }

            $materialissue->deadline = Carbon::now()->addMonths(1);
            $materialissue->save();

            $materials = Material::with('category', 'issued')->withCount('issued')->get();
            $users = User::all();

            $data['materials'] = $materials;
            $data['users'] = $users;

            return json_encode($data);
        }

    }
}
