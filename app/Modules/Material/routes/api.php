<?php

Route::group(['module' => 'Material', 'middleware' => ['api'], 'namespace' => 'App\Modules\Material\Controllers'], function() {

    Route::resource('Material', 'MaterialController');

});
