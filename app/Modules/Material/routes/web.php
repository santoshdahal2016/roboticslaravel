<?php

Route::group(['module' => 'Material', 'middleware' => ['web'], 'namespace' => 'App\Modules\Material\Controllers'], function() {

    Route::get('/material','MaterialController@index');
    Route::get('/material_create','MaterialController@create');
    Route::post('/material_create','MaterialController@store');


    Route::get('/material_edit/{id}','MaterialController@edit');
    Route::post('/material_edit/{id}','MaterialController@update');


    Route::post('/materialcategory_store','MaterialController@store_category');


    Route::post('/material/issue' , 'MaterialController@issue');
    Route::post('/material/return' , 'MaterialController@return');

    Route::post('/material/reissue' , 'MaterialController@reissue');

});
