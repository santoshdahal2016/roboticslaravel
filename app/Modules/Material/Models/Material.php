<?php

namespace App\Modules\Material\Models;

use Illuminate\Database\Eloquent\Model;

class Material extends Model {

    protected $fillable = ['name', 'description', 'quantity','material_category_id','source','damaged'];

    public function category()
    {
        return $this->belongsTo('App\Modules\Material\Models\Materialcategory','material_category_id');
    }

    public function issued(){
        return $this->belongsToMany('App\Modules\User\Models\User','material_issue','materials_id','user_id')->where('status',1)->withPivot('deadline','id');
    }
}
