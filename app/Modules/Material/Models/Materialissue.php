<?php

namespace App\Modules\Material\Models;

use Illuminate\Database\Eloquent\Model;

class Materialissue extends Model
{
    protected  $table = "material_issue";

    protected $fillable = ['user_id', 'materials_id','status','deadline'];

    public function material()
    {
        return $this->belongsTo('App\Modules\Material\Models\Material');
    }

    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }
}
