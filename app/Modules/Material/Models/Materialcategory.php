<?php

namespace App\Modules\Material\Models;

use Illuminate\Database\Eloquent\Model;

class Materialcategory extends Model
{
    protected $table = "material_category";

    protected $fillable = ['name', 'description'];

    public function material()
    {
        return $this->hasMany('App\Modules\Material\Models\Material');
    }

}
