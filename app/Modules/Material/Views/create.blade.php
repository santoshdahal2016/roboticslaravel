@extends( 'User::app' )
@section('styles')
    <link href="{{ url('plugins/bower_components/summernote/dist/summernote.css')}}" rel="stylesheet"/>
@endsection
@section('content-header')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Material</h4></div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i
                        class="ti-settings text-white"></i></button>

            <ol class="breadcrumb">
                <li><a href="#">Material</a></li>
                <li class=""><a href="{{url('/material')}}">Material</a></li>
                <li class="active">Register</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection
@section('content')
    <div class="row">
        @include('User::errors')

        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading"> New Material</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form action="{{ url('/material_create/') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-body">
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group white-box">
                                            <label class="control-label">Name</label>
                                            <input type="text" name="name" id="firstName" class="form-control"
                                                   placeholder="Robotics Arm"></div>
                                    </div>
                                    <!--/span-->


                                    <div class="col-md-6">
                                        <div class="form-group white-box ">
                                            <label class="control-label">Quantity</label>
                                            <input type="text" name="quantity" id="description" class="form-control"
                                            >
                                        </div>
                                        <!--/span-->
                                    </div>

                                    <div class="col-md-6">
                                        <div class="white-box">
                                            <h4>Category</h4>
                                            <select name="material_category_id" id="selectcategory"
                                                    class=" form-group form-control form-control-line" required>
                                                <option value="">Select Category</option>
                                                @foreach($category as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>

                                                @endforeach
                                            </select>
                                            <div class="button-box">
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#exampleModal" data-whatever="@mdo">Add Category
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="white-box">
                                            <h4>Source</h4>
                                            <select name="source" id="selectcategory"
                                                    class=" form-group form-control form-control-line" required>
                                                <option value="">Select Source</option>

                                                    <option value=@php echo \MaterialSource::ROBOTICS ; @endphp>Robotics</option>
                                                <option value=@php echo \MaterialSource::ELECTRONICS_DEPARTMENT ; @endphp>ELECTRONICS DEPARTMENT</option>


                                            </select>

                                        </div>
                                    </div>
                                    <hr>

                                    <div class="col-md-12">
                                        <div class="form-group ">
                                            <label class="control-label">Description</label>
                                            <textarea type="text" name="description" id="description"
                                                      class="form-control"
                                            ></textarea>
                                        </div>
                                        <!--/span-->
                                    </div>

                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save
                                    </button>
                                    <button type="button" class="btn btn-default">Cancel</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="category" action="/materialcategory_store" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel1">New Category</h4></div>
                    <div class="modal-body">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Name:</label>
                            <input type="text" class="form-control" name="name"></div>

                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Description:</label>
                            <input type="text" class="form-control" name="description"></div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ url('plugins/bower_components/summernote/dist/summernote.min.js')}}"></script>
    <script>
        jQuery(document).ready(function () {

            $('textarea').summernote({
                height: 350, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: false // set focus to editable area after initializing summernote
            });
        });

    </script>
    <script>
        $(function () {
            $('#category').on('submit', function (event) {
                event.preventDefault(); //prevent default action
                var post_url = $(this).attr("action"); //get form action url
                var request_method = $(this).attr("method"); //get form GET/POST method
                var form_data = $(this).serialize(); //Encode form elements for submission

                $.ajax({
                    url: post_url,
                    type: request_method,
                    data: form_data
                }).done(function (response) {
                    $("#exampleModal").modal("hide");
                    $("#selectcategory").append('<option value="' + response.id + '">' + response.name + '</option>');
                    $("#myselect").val(response.id);
                    // console.log(response);
                });
            });
        });
    </script>
@endsection