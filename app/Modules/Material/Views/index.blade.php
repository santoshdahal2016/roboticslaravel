@extends( 'User::app' )
@section('content-header')
    <div class="row bg-title" xmlns:v-tooltip="http://www.w3.org/1999/xhtml">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Materials</h4></div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i
                        class="ti-settings text-white"></i></button>

            <ol class="breadcrumb">
                <li><a href="#">Materials</a></li>
                <li class="active">Material</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection
@section('styles')
    <script src="https://unpkg.com/vue@2.5.16/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
    <script type="text/javascript" src="//unpkg.com/uiv/dist/uiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment-with-locales.min.js"></script>
@endsection
@section('content')
    <div id="material">
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="row row-in">
                        <div class="col-lg-3 col-sm-6 row-in-br">
                            <ul class="col-in">
                                <li>
                                    <span class="circle circle-md bg-danger"><i class="ti-clipboard"></i></span>
                                </li>
                                <li class="col-last"><h3 class="counter text-right m-t-15">@{{ materials.length }}</h3>
                                </li>
                                <li class="col-middle">
                                    <h4>Total Material</h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger" role="progressbar"
                                             aria-valuenow="40"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                    <h3 class="box-title"><a href="{{ url('/material_create') }}">
                                            <button class="btn btn-sm btn-success">Add Materials</button>
                                        </a></h3>

                                </li>

                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-6 row-in-br">
                            <ul class="col-in">
                                <li>
                                    <span class="circle circle-md bg-info"><i class="ti-wallet"></i></span>
                                </li>
                                <li class="col-last"><h3 class="counter text-right m-t-15">@{{ total_issue }}</h3></li>
                                <li class="col-middle">
                                    <h4>Total Issue</h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" role="progressbar"
                                             aria-valuenow="40"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-6 row-in-br">
                            <ul class="col-in">
                                <li>
                                    <span class="circle circle-md bg-success"><i class=" ti-shopping-cart"></i></span>
                                </li>
                                <li class="col-last"><h3 class="counter text-right m-t-15">@{{ projects_count }}</h3></li>
                                <li class="col-middle">
                                    <h4>Total Projects</h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" role="progressbar"
                                             aria-valuenow="40"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-6  b-0">
                            <ul class="col-in">
                                <li>
                                    <span class="circle circle-md bg-warning"><i class="fa fa-dollar"></i></span>
                                </li>
                                <li class="col-last"><h3 class="counter text-right m-t-15">@{{ material_status }}</h3></li>
                                <li class="col-middle">
                                    <h4>Material Status</h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning" role="progressbar"
                                             aria-valuenow="40"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('flash')
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="table-responsive manage-table">
                        <table class="table" cellspacing="14">
                            <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th width="150">Name</th>
                                <th>Category</th>
                                <th>Stock</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="(item ,i) in materials " class="advance-table-row ">
                                <td width="10"></td>
                                <td width="10"></td>

                                <td width="40">
                                    <div class="checkbox checkbox-circle checkbox-info">
                                        <input id="checkbox7" type="checkbox">
                                        <label for="checkbox7"> </label>
                                    </div>
                                </td>
                                <td>@{{item.name}}</td>
                                <td>@{{item.category.name}}</td>

                                <td>@{{ item.quantity }}</td>
                                <td>
                                    <btn @click="detail(i)" type="success">Detail</btn>

                                    <a class="btn btn-default" @click="redit(item.id)">Edit</a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" class="sm-pd"></td>
                            </tr>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>


        <modal v-model="open" :title="material_selected.name" ref="modal" id="modal-demo">

            <p v-html="material_selected.description"></p>
            <h4>Total : @{{ material_selected.quantity }} </h4>
            <h4>Damaged : @{{ material_selected.damaged }} </h4>
            <h4>Issued : @{{ material_selected.issued_count }} </h4>

            <table width="100%" class="table table-striped">
                <tr>
                    <th>Name</th>
                    <th>Deadline</th>
                    <th>Action</th>
                </tr>
                <tr v-for="(item1 ,i) in material_selected.issued ">
                    <td>@{{ item1.name }}</td>
                    <td>@{{ item1.pivot.deadline| fromNow }}</td>
                    <td>
                        <btn @click="return_material(item1.pivot.id)" type="warning">Return</btn>
                        <btn @click="reissue_material(item1.pivot.id)" type="success">Reissue</btn>
                    </td>
                </tr>
            </table>
            <hr>
            <div class="form" v-show="enable_issue" v-if="issue">

                <div class="white-box">
                    <div class="form-group">

                        <label for="input">User:</label>
                        <input class="form-control input-sm" list="users" v-model="issue_user">
                        <datalist id="users">
                            <option v-for="user in users" :value="user.email"/>
                        </datalist>

                    </div>
                </div>
                <div class="white-box col-lg-12">
                    <label for="input">Submit Date:</label>

                    <dropdown class="form-group">
                        <div class="input-group">
                            <input class="form-control" type="text" v-model="issue_date">
                            <div class="input-group-btn">
                                <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                            </div>
                        </div>
                        <template slot="dropdown">
                            <li>
                                <date-picker v-model="issue_date"/>
                            </li>
                        </template>
                    </dropdown>
                </div>
                <div class="white-box col-lg-12">
                    <btn @click="issue_material()" type="success">Save</btn>

                </div>
            </div>

            <div slot="footer">
                <btn @click="issue = true" v-show="enable_issue" type="success">Issue</btn>
            </div>
        </modal>
    </div>
@endsection

@section('scripts')



    <script type="text/javascript">


        var rr = new Vue({
            el: '#material'
            ,
            components: {},
            data: function () {
                return {

                    materials: {!! $materials !!},
                    users: {!! $users !!},
                    open: false,
                    selected: 0,
                    issue_date: null,
                    issue: false,
                    issue_user: null,
                    projects_count : {!! $projects !!},
                    edit : "/material_edit/"

                }
            },
            computed: {
                material_selected: function () {
                    return this.materials[this.selected];
                },
                enable_issue: function () {
                    if (this.materials[this.selected].quantity > this.materials[this.selected].issued_count) {
                        return true;

                    } else {
                        this.issue = false;

                        return false;
                    }
                },
                total_issue: function () {
                    var total = 0;
                    this.materials.forEach(function (material) {

                        total += material.issued_count;

                    });

                    return total;

                }
                ,
                material_status: function () {
                    var total = 0;
                    var damage = 0;
                    this.materials.forEach(function (material) {

                        total += material.quantity;
                        damage += material.damaged;

                    });

                    return Number((100 - ((damage/total) * 100)).toFixed(1));

                }
            }
            ,
            filters: {
                format(date) {
                    return moment(date).format('YYYY-MM-DD')
                },
                fromNow(date) {
                    return moment(date).fromNow();
                }
            },
            methods: {

                detail(i) {
                    this.selected = i;
                    this.open = true;
                },
                moment(...args) {
                    return moment(...args);
                },
                issue_material() {

                    axios.post('/material/issue', {
                        'user_email': this.issue_user,
                        'material_id': this.material_selected.id,
                        'deadline': this.issue_date
                    })
                        .then(res => {
                            // console.log(res.data)
                            this.materials = res.data.materials;
                            this.users = res.data.users;
                            this.issue_date = null;
                            this.issue = null;

                            this.$notify({
                                title: 'Issued',
                                content: this.material_selected.name + " issued to " + this.issue_user,
                            });
                            this.issue_user = null;

                        })
                        .catch(err => {
                            this.$notify({
                                type: 'warning',
                                title: 'Warning!',
                                content: 'Please check your input and try again Or Refresh Once.'
                            })
                        })
                },
                return_material(id) {
                    this.$confirm({
                        title: 'Confirm',
                        content: 'This item is returned. Continue?'
                    })
                        .then(() => {
                            axios.post('/material/return', {
                                'id': id,
                            })
                                .then(res => {
                                    // console.log(res.data)
                                    this.materials = res.data.materials;
                                    this.users = res.data.users;
                                    this.issue_date = null;
                                    this.issue = null;

                                    this.$notify({
                                        title: 'Return',
                                        content: this.material_selected.name + " is returned  ",
                                    });
                                    this.issue_user = null;

                                })
                                .catch(err => {
                                    this.$notify({
                                        type: 'warning',
                                        title: 'Warning!',
                                        content: 'Please check your input and try again Or Refresh Once.'
                                    })
                                })
                        })
                        .catch(() => {
                            this.$notify('Return canceled.')
                        })


                },
                reissue_material(id) {
                    this.$prompt({
                        title: 'Extend for one month',
                        content: 'Please input  email (Taker email):',
                        // A simple input validator
                        // returns the err msg (not valid) or null (valid)
                        validator (value) {
                            return /\S+@\S+\.\S+/.test(value) ? null : 'Email address is not valid!'
                        }
                    })
                        .then((value) => {
                            axios.post('/material/reissue', {
                                'id': id,
                                'email' : value
                            })
                                .then(res => {
                                    // console.log(res.data)
                                    this.materials = res.data.materials;
                                    this.users = res.data.users;
                                    this.issue_date = null;
                                    this.issue = null;

                                    this.$notify({
                                        title: 'Reissue',
                                        content: this.material_selected.name + " is Reissue  ",
                                    });
                                    this.issue_user = null;

                                })
                                .catch(err => {
                                    console.log(err);
                                    this.$notify({
                                        type: 'warning',
                                        title: 'Warning!',
                                        content: 'Please check your Email and try again Or Refresh Once.'
                                    })
                                })
                        })
                        .catch(() => {
                            this.$notify('Input canceled.')
                        })
                },
                redit(id){
                    window.location.href = this.edit + id;
                }

            }
        });
    </script>

@endsection



