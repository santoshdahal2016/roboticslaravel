<?php

namespace App\Modules\Frontend\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

class MessageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('create');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = \App\Modules\Frontend\Models\Message::select('name')->distinct()->addSelect('year','faculty')->orderBy('faculty')->get();
        return view("Frontend::message.index")->with(['messages' => $messages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'faculty' => 'required',
            'year' => 'required',
            'body' =>'required'

        ]);
        if ($validator->fails()) {
            $return = ["status" => "error",
                "error" => [
                    "code" => 422,
                    "errors" => $validator->errors()
                ]];

            return response()->json($return, 422);

        }
        $message['name'] = $request->name;
        $message['faculty'] = $request->faculty;
        $message['year'] = $request->year;
        $message['body'] = $request->body;
        $message['type'] = "RAINY SESSION";

        if(\App\Modules\Frontend\Models\Message::create($message)){
            return json_encode($message);
        }else{
            $return = ["status" => "error",
                "error" => [
                    "code" => 422,
                    "errors" => "Message not created"
                ]];

            return response()->json($return, 422);
        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
