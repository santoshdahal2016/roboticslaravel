<?php

Route::group(['module' => 'Frontend','prefix' =>'api', 'middleware' => ['api','cros'], 'namespace' => 'App\Modules\Frontend\Controllers'], function() {

    Route::post('message', 'MessageController@create');

});
