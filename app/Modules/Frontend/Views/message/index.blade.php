@extends( 'User::app' )
@section('content-header')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Message</h4></div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i
                        class="ti-settings text-white"></i></button>

            <ol class="breadcrumb">
                <li><a href="#">Message</a></li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">

                <div class="table-responsive manage-table">
                    <table class="table" cellspacing="14">
                        <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th width="150">Name</th>
                            <th width="150">Faculty</th>
                            <th width="150">Year</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($messages as $item)
                            <tr class="advance-table-row ">
                                <td width="10"></td>
                                <td width="10"></td>

                                <td width="40">
                                    <div class="checkbox checkbox-circle checkbox-info">
                                        <input id="checkbox7" type="checkbox">
                                        <label for="checkbox7"> </label>
                                    </div>
                                </td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->faculty }}</td>
                                <td>{{ $item->year }}</td>

                            </tr>
                            <tr>
                                <td colspan="7" class="sm-pd"></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

@endsection



