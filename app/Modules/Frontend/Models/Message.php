<?php

namespace App\Modules\Frontend\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model {

    protected $fillable = ['name', 'faculty', 'year','type','body'];


}
