<?php

namespace App\Modules\Event\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{

    protected $table = "sessions";

    public function projects()
    {
        return $this->belongsToMany('App\Modules\Project\Models\Project' ,'sessions_projects');
    }

}
