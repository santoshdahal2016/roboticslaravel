<?php

namespace App\Modules\Project\Controllers;

use App\Modules\Chat\Models\Conversation;
use App\Modules\Chat\Models\Group;
use App\Modules\Project\Models\Phase;
use App\Modules\Project\Models\Project;
use App\Modules\Project\Models\Task;
use App\Modules\Project\Models\UserProject;
use App\Modules\User\Models\Role;
use App\Modules\User\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $projects = Project::where('user_id', $user->id)->get();
        return view("Project::index")->with(['projects' => $projects]);
    }

    public function create()
    {
        $user = Auth::user();
        if ($user->hasRole('developer') || $user->can('project')) {
            return view('Project::create');
        } else {
            abort(403);
        }

    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        if ($user->hasRole('developer') || $user->can('project')) {
            $this->validate($request, [
                'name' => 'required|min:3',
                'description' => 'required',

            ]);
            $project['user_id'] = $user->id;
            $project['name'] = $request['name'];
            $project['description'] = $request['description'];

            $project = Project::create($project);

            $group = Group::create(['name' => $project->id]);

            $group->users()->attach($user->id);

            return redirect('/project/');
            // }
        } else {
            abort(403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //For authentications
        $logged = Auth::user();
        $project = Project::where('id', $id)->first();

//        dd($project->tasks->first());
        if ($project->user_id != $logged->id) {


            $collaboration = UserProject::where('user_id', $logged->id)->where('projects_id', $id)->first();
            if (!isset($collaboration)) {
                abort(403);
            }
            $role = Role::where('id', $collaboration->role_id)->first();
        } else {

            $role = null;
        }

        //For Progress Board
        $master = User::where('id', $project->user_id)->pluck('id');
        $other = UserProject::where('projects_id', $id)->with('role', 'user')->pluck('user_id');
        $all = $other->concat($master);
        $all = User::whereIn('id', $all->toArray())->get()->toArray();
        $phase = Phase::where('projects_id', $project->id)->orderBy('sequence')->with('task')->get()->toArray();

        foreach ($phase as $key => $item) {
            $board[$key] = $item;

            foreach ($all as $key1 => $item1) {
                $board[$key]['member'][$key1] = $item1;
                $tasks = Task::where('phases_id', $item['id'])->whereHas('users', function($query) use ($item1)
                {
                    $query->where('users.id',  $item1['id']);
                })->get()->toArray();
//                dd($tasks);

                foreach ($tasks as $key2 => $item2) {
                    $board[$key]['member'][$key1]['task'][$key2] = $item2;
                    $pivot = DB::table('users_tasks')->where('user_id',$item1['id'])->where('tasks_id',$item2['id'])->first();

                    $board[$key]['member'][$key1]['task'][$key2]['status'] =  $pivot->status;

//                    dd($pivot);
                    $progress = DB::table('task_report')->where('users_tasks_id',$pivot->id)->get();
//                    dd($progress);

                    foreach ($progress as $key3 => $item3) {

                        $board[$key]['member'][$key1]['task'][$key2]['progress'][] = $item3;
                    }

                }
            }
        }
//        dd($board);
        //For Task Tab
        $phase = Phase::where('projects_id', $project->id)->with('task')->get();
        $task = '[';

        foreach ($phase as $item) {
            $task .= ' {text: "' . $item->name . '" , nodes: [';
            if (isset($item->task)) {
                foreach ($item->task as $item1) {
                    $task .= '{text: "' . $item1->name . '"},';
                }
            }
            $task .= ']},';
        }
        $task .= ']';

        //For Member Tab
        $user = User::where('id', $project->user_id)->first();
        $members = UserProject::where('projects_id', $id)->with('role', 'user')->get();


        //For progress entry
        $phase = Phase::where('projects_id', $project->id)->pluck('id')->toArray();

        $tasklist =Task::whereIn('phases_id', $phase)->whereHas('users', function($query) use ($item1 ,$logged)
        {
            $query->where('users.id',  $logged->id);
        })->pluck('name', 'id')->all();

        $group = Group::where('name' , $id)->first();
        $talk = Conversation::where('group_id',$group->id)->with('user')->take('30')->orderBy('id', 'DESC')->get();
        return view('Project::view')->with(['project' => $project
            , 'members' => $members
            , 'user' => $user
            , 'task' => $task
            ,'board' => $board
            ,'tasklist' => $tasklist,
            'talk' => $talk
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     * Save change in colaboration
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function report_save(Request $request)
    {
        $task = Task::where('id', $request->task_id)->first();
        $user =Auth::user();
        $pivot = DB::table('users_tasks')->where('user_id',$user->id)->where('tasks_id',$task->id)->first();
        $report = DB::table('task_report')->where('users_tasks_id',$pivot->id)->whereRaw('Date(created_at) = CURDATE()')->first();
//        dd($report);
        if (isset($report)) {
            $data['name'] = $request->name;
            $data['description'] = $request->report;
            $data['updated_at'] = Carbon::now();;
            DB::table('task_report')->where('id',$report->id)->update($data);

            $header = "From: ROBOTICS <robotics@wrc.edu.np>\r\n";



        } else {

            $data['name'] = $request->name;
            $data['description'] = $request->report;
            $data['users_tasks_id'] = $pivot->id;
            $data['created_at'] = Carbon::now();;

            DB::table('task_report')->insert($data);

        }
        return redirect()->back();
    }

    public function task_detail(Request $request)
    {
        $task = Task::where('id', $request->selecttask)->first();
        $user =Auth::user();
        $pivot = DB::table('users_tasks')->where('user_id',$user->id)->where('tasks_id',$task->id)->first();
        $report = DB::table('task_report')->where('users_tasks_id',$pivot->id)->whereRaw('Date(created_at) = CURDATE()')->first();
        $data['name'] = $report->name;
        $data['description'] = $report->description;
        return response()->json($data);
    }

    /**
     * Delete collaboration
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    /**
     * Save change in colaboration
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function collaboration_save(Request $request)
    {
        $project = Project::where('id', $request->project)->first();
        $logged = Auth::user();
        if($logged->id != $project->user_id){
            abort(403);
        }
        exit();
        $user = User::where('email', $request->email)->first();

        if (!isset($user)) {

            session()->flash('error', 'No user found with this email!');

            return redirect()->back();

        }
        $user_project = UserProject::where('projects_id', $project->id)->where('user_id', $user->id)->first();

        if (isset($user_project)) {
            $data['projects_id'] = $project->id;
            $data['user_id'] = $user->id;
            $data['role_id'] = $request['role_id'];
            $user_project->fill($data)->save();
        } else {
            $data['projects_id'] = $project->id;
            $data['user_id'] = $user->id;
            $data['role_id'] = $request['role_id'];
            UserProject::create($data);
        }
        $group = Group::where('name', $project->id)->first();

        $group->users()->attach($user->id);

        session()->flash('success', 'Member have been added');

        return redirect()->back();
    }

    /**
     * Delete collaboration
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function collaboration_delete($id)
    {
        $project_user = UserProject::findOrFail($id);
        $user = Auth::user();
        if ($user->hasRole('developer') || $user->id == $project_user->project->user->id) {
            $project_user->delete();
            session()->flash('error', 'Member have been removed');
            return redirect()->back();
        } else {

            abort(403);
        }

    }
}
