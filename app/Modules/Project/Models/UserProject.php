<?php

namespace App\Modules\Project\Models;

use Illuminate\Database\Eloquent\Model;

class UserProject extends Model
{
    protected $table = 'user_projects';

    protected $fillable = ['role_id', 'projects_id', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User', 'user_id');
    }
    public function role()
    {
        return $this->belongsTo('App\Modules\User\Models\Role', 'role_id');
    }
    public function project()
    {
        return $this->belongsTo('App\Modules\Project\Models\Project', 'projects_id');
    }
}
