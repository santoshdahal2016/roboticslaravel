<?php

namespace App\Modules\Project\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

    protected $table = 'projects';

    protected $fillable = ['name', 'description', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User', 'user_id');
    }


    public function phase()
    {
        return $this->hasMany('App\Modules\Project\Models\Phase');
    }

    public function tasks(){
        return $this->hasManyThrough('App\Modules\Project\Models\Task' ,'App\Modules\Project\Models\Phase','projects_id','phases_id');

    }

}
