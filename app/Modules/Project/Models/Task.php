<?php

namespace App\Modules\Project\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';

    protected $fillable = ['name', 'description', 'phases_id', 'sequence', 'deadline'];

    public function phase()
    {
        return $this->belongsTo(' App\Modules\Project\Models\Phase', 'phase_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\Modules\User\Models\User', 'users_tasks',
            'tasks_id', 'user_id')->withPivot('status','id')->withTimestamps();
    }
}
