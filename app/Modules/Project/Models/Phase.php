<?php

namespace App\Modules\Project\Models;

use Illuminate\Database\Eloquent\Model;

class Phase extends Model
{
    protected $table = 'phases';

    protected $fillable = ['name', 'description', 'projects_id', 'sequence', 'deadline'];

    public function project()
    {
        return $this->belongsTo('App\Modules\Project\Models\Project', 'projects_id');
    }

    public function task(){
        return $this->hasMany('App\Modules\Project\Models\Task', 'phases_id');

    }
}
