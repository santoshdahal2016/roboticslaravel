@extends( 'User::app' )
@section('styles')
    <link href="{{ url('plugins/bower_components/summernote/dist/summernote.css')}}" rel="stylesheet"/>
@endsection
@section('content-header')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Project</h4></div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i
                        class="ti-settings text-white"></i></button>

            <ol class="breadcrumb">
                <li><a href="#">Project</a></li>
                <li class=""><a href="{{url('/project')}}">Project</a></li>
                <li class="active">Register</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection
@section('content')

    <div class="row">
        @include('User::errors')

        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading"> New Project</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <project_create></project_create>

                        <form action="{{ url('/project_create/') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-body">
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Name</label>
                                            <input type="text" name="name" id="firstName" class="form-control"
                                                   placeholder="Robotics Arm"></div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-12">
                                        <div class="form-group ">
                                            <label class="control-label">Detail</label>
                                            <textarea type="text" name="description" id="description" class="form-control"
                                                     ></textarea>
                                    </div>
                                    <!--/span-->
                                </div>

                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
                                <button type="button" class="btn btn-default">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ url('plugins/bower_components/summernote/dist/summernote.min.js')}}"></script>
    <script>
        jQuery(document).ready(function () {

            $('textarea').summernote({
                height: 350, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: false // set focus to editable area after initializing summernote
            });
        });

    </script>
@endsection