@extends( 'User::app' )
@section('content-header')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">User</h4></div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i
                        class="ti-settings text-white"></i></button>

            <ol class="breadcrumb">
                <li><a href="#">Users</a></li>
                <li class=""><a href="{{url('/user')}}">Users</a></li>
                <li class="active">Edit</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection
@section('content')
    <div class="row">
        @include('User::errors')

        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading"> Edit User</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::model($user, ['method' => 'post','url' => '/user_edit/'.$user->id,'class'=>'']) !!}
                        {{ csrf_field() }}
                        <div class="form-body">
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Name</label>
                                        {!! Form::text('name', null, ['placeholder'=>'name','required'=>'required','class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label class="control-label">Email</label>
                                        {!! Form::email('email', null, ['placeholder'=>'name','required'=>'required','class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label class="control-label">Password</label>
                                        {!! Form::password('password', ['placeholder'=>'password','required'=>'required','class'=>'form-control']) !!}
                                    </div>

                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Confirm Password</label>
                                        <input type="password" required='required' class="form-control" name="password_confirmation"
                                               id="inputPasswordC3"
                                               placeholder="Confirm Password">
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Role:</label>

                                        {!! Form::select('roles_id',$all_roles, $selected_role, ['class' => 'form-control','required'=>'required']) !!}

                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">

                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->

                            <!--/row-->

                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
                            <button type="button" class="btn btn-default">Cancel</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /.row -->
@endsection