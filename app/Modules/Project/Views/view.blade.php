@extends( 'User::app' )
@section('styles')
    <link href="{{url('plugins/bower_components/sweetalert/sweetalert.css')}}" rel="stylesheet"/>
    <link href="{{url('plugins/bower_components/summernote/dist/summernote.css')}}" rel="stylesheet"/>
    <link href="../plugins/bower_components/summernote/dist/summernote.css" rel="stylesheet"/>
<style>
    * {
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }
    .container {
        width: 400px;
        margin: 0 auto;
        border: solid 1px #ccc;
        border-radius: 5px;
        overflow: hidden;
    }
    .chat-container {
        height: 400px;
        overflow: auto;
        -webkit-transform: rotate(180deg);
        transform: rotate(180deg);
        direction: rtl;
    }
    .chat-container .message {
        border-bottom: solid 1px #ccc;
        padding: 20px;
        -webkit-transform: rotate(180deg);
        transform: rotate(180deg);
        direction: ltr;
    }
    .chat-container .message .avatar {
        float: left;
        margin-right: 5px;
    }
    .chat-container .message .datetime {
        float: right;
        color: #999;
    }
    .send-message-form input {
        width: 100%;
        border: none;
        font-size: 16px;
        padding: 10px;
    }
    .send-message-form button {
        display: none;
    }

</style>
@endsection
@section('content-header')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ $project->name }}</h4></div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i
                        class="ti-settings text-white"></i></button>

            <ol class="breadcrumb">
                <li><a href="#">Projects</a></li>
                <li class="active">Projects</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection

@section('content')
    <section>
        <div class="sttabs tabs-style-bar">
            <nav>
                <ul>
                    <li><a href="#section-bar-1" class="sticon ti-stats-up"><span>Progress Board</span></a></li>
                    <li><a href="#section-bar-2" class="sticon ti-trash"><span>Tasks</span></a></li>
                    <li><a href="#section-bar-3" class="sticon ti-home"><span>Resources</span></a></li>
                    <li><a href="#section-bar-4" class="sticon ti-upload"><span>Member</span></a></li>
                    <li><a href="#section-bar-5" class="sticon ti-settings"><span>Chat</span></a></li>
                </ul>
            </nav>
            <div class="content-wrap">
                <section id="section-bar-1">
                    @foreach($board as $key => $item)
                        <div class="row">
                            <div class="panel-group" id="acc1{{$key}}{{$item['name']}}"
                                 aria-multiselectable="true" role="tablist">
                                <div class="panel ">
                                    <div class="panel-heading @if( $item['status'] == 'finished')
                                            bg-success
@elseif($item['status'] == 'running')
                                            bg-info
@else
                                            bg-danger
@endif " id="htype{{$key}}" role="tab"><a
                                                class="panel-title" data-toggle="collapse" href="#ctype{{$key}}"
                                                data-parent="#exampleAccordionDefault" aria-expanded="true"
                                                aria-controls="exampleCollapseDefaultOne">Phase {{$key +1}}
                                            : {{$item['name']}}</a>
                                    </div>
                                    <div class="panel-collapse collapse @if($key +1 == count($board))  in @endif "
                                         id="ctype{{$key}}"
                                         aria-labelledby="htype{{$key}}" role="tabpanel">
                                        <div class="panel-body">
                                            @foreach($item['member'] as $key1 => $item1)
                                                <div class="col-sm-6">
                                                    <div class="white-box">
                                                        <h3 class="box-title m-b-0">{{$item1['name']}} , {{$item1['faculty']}} ,{{$item1['year']}}</h3>
                                                        <small>Progress Report</small>
                                                        <hr>
                                                        <div class="panel-group" id="acc1{{$key1}}{{$item1['name']}}"
                                                             aria-multiselectable="true" role="tablist">
                                                            @if(isset($item1['task']))
                                                                @foreach($item1['task'] as $key2 => $item2)

                                                                    <div class="panel @if( $item2['status'] == 'finished')
                                                                            panel-success
@elseif($item2['status'] == 'running')
                                                                            panel-info
@else
                                                                            panel-danger
@endif ">
                                                                        <div class="panel-heading "
                                                                             id="htype1{{$key1}}{{$key2}}"
                                                                             role="tab"><a class="panel-title"
                                                                                           data-toggle="collapse"
                                                                                           href="#ctype1{{$key1}}{{$key2}}"
                                                                                           data-parent="#acc1{{$key1}}{{$key2}}"
                                                                                           aria-expanded="true"
                                                                                           aria-controls="ctype1{{$key1}}{{$key2}}">
                                                                              Task {{$key2 +1 }} : {{$item2['name']}} </a></div>

                                                                        <div class="panel-collapse collapse @if($key2 +1 == count($item1['task']))  in @endif"
                                                                             id="ctype1{{$key1}}{{$key2}}"
                                                                             aria-labelledby="htype1{{$key1}}{{$key2}}"
                                                                             role="tabpanel">
                                                                            <div class="panel-body">
                                                                                @if(isset($item2['progress']))

                                                                                    @foreach($item2['progress'] as $key3 => $item3)
                                                                                        <span ><p><span class="badge bg-green">{{$item3->name}} : </span> <small class="text-muted"><i class="fa fa-clock-o"></i>@php echo date('"F jS, Y"', strtotime($item3->created_at)); @endphp   </small> </p></span>
                                                                                         {!! $item3->description !!}
                                                                                        <br>
                                                                                    @endforeach
                                                                                @else
                                                                                    User have not started yet.
                                                                                @endif

                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                @endforeach
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="panel panel-success">
                                <div class="panel-heading">Your Progress</div>
                                <div class="panel-wrapper collapse in" aria-expanded="true">
                                    <div class="panel-body">
                                        <form action="{{ url('/project_progress/') }}" id="add" method="post"
                                              class="create"
                                              enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="project" value="{{$project->id}}">

                                            <div class="form-group form-float">
                                                <label class="form-label">Select Task*</label>
                                                {!! Form::select('task_id',['' => 'Select Task']+$tasklist , null, ['class'=>'form-control ms selectpicker','onChange'=>'task1(this)','id'=>'task','required'=>'required']) !!}

                                            </div>
                                            <div class="form-group form-float">
                                                <label class="form-label">Name*</label>
                                                <input type="text" class="form-control "  onclick="task2()" id="reportname" required name="name">
                                            </div>

                                            <label class="form-label">Detail</label>

                                            <textarea id="reportdetail" class="form-control " required name="report">
                                </textarea>
                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-success"><i
                                                            class="fa fa-check"></i> Save
                                                </button>
                                                <button type="button" class="btn btn-default">Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="section-bar-2">
                    <div id="tree"></div>


                </section>
                <section id="section-bar-3">
                </section>
                <section id="section-bar-4">
                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error')}}
                        </div>
                    @endif
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">Add | Edit Member</div>
                                <div class="panel-wrapper collapse in" aria-expanded="true">
                                    <div class="panel-body">
                                        <form action="{{ url('/project_collaboration/') }}" id="add" method="post"
                                              class="create"
                                              enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="project" value="{{$project->id}}">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="form-label">Email of Member to invite*</label>

                                                    {!! Form::text('email', null, ['class'=>'form-control','onkeyup'=>'change(this)','id'=>'inputTitle','required'=>'required']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                <label class="form-label">Select Role*</label>
                                                {!! Form::select('role_id',['' => 'Select Type','2'=>'User'] , null, ['class'=>'form-control ms selectpicker','id'=>'type','required'=>'required']) !!}

                                            </div>
                                            <button type="submit" class="btn btn-info ">Add to Project</button>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive manage-table">
                        <table class="table" cellspacing="2">
                            <thead>
                            <tr>
                                <th width="150">Name</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="advance-table-row ">
                                <td>{{ $user->name }}</td>
                                <td>Master</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="sm-pd"></td>
                            </tr>
                            @foreach($members as $item)
                                <tr class="advance-table-row ">
                                    <td>{{ $item->user->name }}</td>
                                    <td>{{ $item->role->display_name }}</td>
                                    <td>
                                        <a id="delete" class=" waves-effect "
                                           href="javascript:void()" onclick="delete_member({{$item->id}})">
                                            <button type="button"
                                                    class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
                                                        class="ti-trash"></i></button>
                                        </a></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="sm-pd"></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </section>
                <section id="section-bar-5">
                        <div class="panel panel-info">
                            <div class="panel-heading"> CHATBOX
                                <div class="pull-right"> <a href="javascript:void(0)" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">

                                        <div class="chat-container">
                                            @if(count($talk) > 0)
                                                @foreach($talk as $item)
                                                    <div class="message" >
                                                        <div class="chat-body">
                                                            <div class="chat-text">
                                                                <h4 class="badge">{{$item->user->name}} :</h4>
                                                                <p class="">{{$item->message}} </p> </div>
                                                        </div>
                                                    </div>
                                                @endforeach

                                            @else
                                            <div class="message" >
                                                <div class="chat-body">
                                                    <div class="chat-text">
                                                        <h4 class="badge">Admin :</h4>
                                                        <p class=""> Hi, All! </p> </div>
                                                </div>
                                            </div>
                                             @endif
                                        </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <form id="message" method="post" action="/conversations">
                                            {{ csrf_field() }}
                                        <div class="col-xs-8">
                                            <input type="text" placeholder="Type your message here" name="message" id="chat-box-input" class="chat-box-input"></input>
                                        </div>
                                            <input type="hidden" name="group_id" value="{{$project->id}}">
                                        <div class="col-xs-4 text-right">
                                            <button class="btn btn-success btn-circle btn-xl" type="submit"><i class="fa fa-paper-plane-o"></i></button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- /.col -->

                </section>
            </div>
            <!-- /content -->
        </div>
        <!-- /tabs -->
    </section>

@endsection

@section('scripts')
    <script src="{{url('js/cbpFWTabs.js')}}"></script>
    <script type="text/javascript">
        (function () {
            [].slice.call(document.querySelectorAll('.sttabs')).forEach(function (el) {
                new CBPFWTabs(el);
            });
        })();
    </script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js "></script>
    <script src="{{url('plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}}"></script>
    <script src="{{url('plugins/bower_components/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{url('plugins/bower_components/bootstrap-treeview-master/dist/bootstrap-treeview.min.js')}}"></script>
    <script>
        function delete_member(x) {
            swal({
                    title: "Are you sure?",
                    text: " ",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-danger',
                    confirmButtonText: 'Ok!'
                },
                function () {

                    window.location.href = "{{ url('/project_collaboration/delete/') }}/" + x;

                });
        }

        $('#tree').treeview({data: <?php echo $task; ?>});
    </script>
    <script src="../plugins/bower_components/summernote/dist/summernote.min.js"></script>
    <script>
        jQuery(document).ready(function () {

            // $('textarea#reportdetail').summernote({
            //     height: 350, // set editor height
            //     minHeight: null, // set minimum height of editor
            //     maxHeight: null, // set maximum height of editor
            //     focus: false // set focus to editable area after initializing summernote
            // });
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function task2() {
            $('textarea#reportdetail').summernote({
                height: 350, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: false // set focus to editable area after initializing summernote
            });
        }
        function task1(select) {

            $.ajax({
                type: "POST",
                url: "/task_detail",
                data: {'selecttask': select.value , 'project': {{$project->id}} },
                success: function (json) {
                    console.log(json);
                    $('#reportname').val(json['name']);
                    // $('textarea#reportdetail').summernote('disable');
                    $('textarea#reportdetail').html(json['description']);
                    $('textarea#reportdetail').summernote({
                        height: 350, // set editor height
                        minHeight: null, // set minimum height of editor
                        maxHeight: null, // set maximum height of editor
                        focus: false // set focus to editable area after initializing summernote
                    });
                },
                fail: function (json) {
                    $('textarea#reportdetail').summernote({
                        height: 350, // set editor height
                        minHeight: null, // set minimum height of editor
                        maxHeight: null, // set maximum height of editor
                        focus: false // set focus to editable area after initializing summernote
                    });
                }
            });
        }
        $('#chat-box').slimScroll({
            color: '#00f',
            size: '10px',
            height: '180px',
            alwaysVisible: true
        });


    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>

    <script>
        var socket = io.connect('https://suchana.herokuapp.com/');

        socket.on('private-groups'+{{$project->id}}, function (data) {

            console.log(data);
            data = jQuery.parseJSON(data);
            var message = $('.message').first().clone();
            message.find('p').text(data.message);
            message.find('h4').text(data.user+ ':');
            message.prependTo('.chat-container');

        });
    </script>

    {{--<script src="https://js.pusher.com/4.2/pusher.min.js"></script>--}}



    {{--<script>--}}

        {{--// Enable pusher logging - don't include this in production--}}
        {{--// Pusher.logToConsole = true;--}}

        {{--var pusher = new Pusher('647cae4dd7e6f096c158', {--}}
            {{--authEndpoint: '/pusher/auth',--}}
            {{--auth: {--}}
                {{--headers: {--}}
                    {{--'X-CSRF-Token': '{{ csrf_token() }}'--}}
                {{--}--}}
            {{--},--}}
            {{--encrypted: true,--}}
            {{--cluster: 'ap2',--}}

        {{--});--}}



        {{--var channel = pusher.subscribe('private-groups.1');--}}
        {{--channel.bind('App\\Events\\NewMessage', function(data) {--}}
            {{--var message = $('.message').first().clone();--}}
            {{--message.find('p').text(data.message);--}}
            {{--message.find('h4').text(data.user.name + ':');--}}
            {{--message.prependTo('.chat-container');--}}
        {{--});--}}
    {{--</script>--}}
    <script>
        $(function () {
            $('#message').on('submit', function (event) {
                event.preventDefault(); //prevent default action
                var post_url = $(this).attr("action"); //get form action url
                var request_method = $(this).attr("method"); //get form GET/POST method
                var form_data = $(this).serialize(); //Encode form elements for submission

                $.ajax({
                    url : post_url,
                    type: request_method,
                    data : form_data
                }).done(function(response){ //
                    $('#chat-box-input').val(" ");
                });
            });
        });
    </script>
@endsection

