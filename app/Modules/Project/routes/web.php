<?php

Route::group(['module' => 'Project', 'middleware' => ['web'], 'namespace' => 'App\Modules\Project\Controllers'], function() {

    Route::get('/project','ProjectController@index');
    Route::get('/project_create','ProjectController@create');
    Route::post('/project_create','ProjectController@store');
    Route::post('/project_progress/','ProjectController@report_save');

    Route::post('/task_detail/','ProjectController@task_detail');


    Route::get('/project/{id}','ProjectController@show');
    Route::post('/project_collaboration', 'ProjectController@collaboration_save');
    Route::get('/project_collaboration/delete/{id}', 'ProjectController@collaboration_delete');

    Route::get('/{project}/phase','PhaseController@index');
    Route::get('/{project}/phase_create','PhaseController@create');
    Route::post('/{project}/phase_create','PhaseController@store');


    Route::get('/{phase}/task','TaskController@index');
    Route::get('/{phase}/task_create','TaskController@create');
    Route::post('/{phase}/task_create','TaskController@store');


});
