<?php

Route::group(['module' => 'Project', 'middleware' => ['api'], 'namespace' => 'App\Modules\Project\Controllers'], function() {

    Route::resource('Project', 'ProjectController');

});
