<?php

namespace App\Http\Controllers\Auth;

use App\Modules\Event\Models\Session;
use App\Modules\Project\Models\Project;
use App\Modules\Project\Models\UserProject;
use App\Modules\User\Models\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\Modules\User\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function showRegistrationForm()
    {
        $projects = Session::where('active', 1)->with('projects')->first();

//        dd($projects->projects);

        return view('auth.register')->with(['projects' => $projects->projects]);
    }


    public function register(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
            'project_id' => 'required',
            'type' => "required|numeric"
        ]);

        $user_input = $request->all();
        $session = Session::where('active', 1)->with('projects')->first();

        $user_input['password'] = bcrypt($user_input['password']);
        $user_input['sessions_id'] = $session->id;

        $created_user = User::create($user_input);

        $created_user->roles()->attach($request['type']);


        $data['projects_id'] = $request['project_id'];
        $data['user_id'] = $created_user->id;
        $data['role_id'] = 2;
        UserProject::create($data);

        $project = Project::where('id', $request['project_id'])->first();

        $data1['user_id'] = $created_user->id;
        $data1['tasks_id'] = $project->tasks->first()->id;
        $data1['status'] = "started";

        DB::table('users_tasks')->insert([
            ['user_id' =>  $data1['user_id'], 'tasks_id' =>  $data1['tasks_id'] , 'status'=>"started"],
        ]);

        return redirect('/wait/');
        // }

    }


     public function wait(){

         return view('auth.wait');

     }
}
