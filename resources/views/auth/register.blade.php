<!DOCTYPE html>
<html>
<head>
    <title>Robotics Registration</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('assets/img/favicon.ico') }}" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/fonts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.1/parsley.min.js"></script>
    <script src="{{ asset('assets/js/ui.js') }}"></script>

    <style>
        body {
            background: url("{{ asset('assets/img/galactic_star_background.png') }}") no-repeat;
            background-size: cover;
        }
    </style>
</head>
<body>


<div id="sheet" class="animated"></div>
<div id="pop-up-prompt" class="animated">
    <header><h3 class="color-badge"></h3></header>
    <div>
        <p></p>
        <section>
            <span id="cancel-btn" class="btn"></span>
            <span id="confirm-btn" class="btn"></span>
        </section>
    </div>
</div>

{{-- HEADER --}}

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="special-form">
                <a href="{{ url('/') }}"><img
                            src="http://www.roboticsconference.org/images/rsslogotext-white.png"
                            alt=""></a>
                <h3 class="text-center">Registration</h3>
                @if ($errors->first())
                    <span class="status-msg error-msg">{{ $errors->first() }}</span>
                @endif
                <hr>
                <form id="sign_in" action="{{ url('/register') }}" method="POST" data-parsley-validate>
                    {{ csrf_field() }}


                    <div class="form-group">
                        <label for="name" class="color-primary">Name:</label>
                        {!! Form::text( 'name', null, array('class' => 'form-control', "placeholder" => "Name","autofocus" => "true" ,"required"=>"required")) !!}
                    </div>

                    <div class="form-group">
                        <label for="email" class="color-primary">Email:</label>
                        {!! Form::email( 'email', null, array('class' => 'form-control', "placeholder" => "Email","autofocus" => "true" ,"required"=>"required")) !!}
                    </div>

                    <div class="form-group">
                        <label for="email" class="color-primary">Faculty:</label>
                        <select name="faculty" id="type"
                                class=" form-group form-control form-control-line" required>
                            <option value="">Select Faculty</option>
                            <option value="ELECTRONICS">Electronics</option>
                            <option value="MECHANICAL">Mechanical</option>
                            <option value="ELECTRICAL">Electrical</option>
                            <option value="COMPUTER">Computer</option>
                            <option value="CIVIL">Civil</option>
                            <option value="GEOMETICS">Geomatics</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="email" class="color-primary">Year:</label>
                        <select name="year" id="type"
                                class=" form-group form-control form-control-line" required>
                            <option value="">Select Year</option>
                            <option value="1">First</option>
                            <option value="2">Second</option>
                            <option value="3">Third</option>
                            <option value="4">Fourth</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="phone" class="color-primary">Phone:</label>
                        {!! Form::number( 'phone', null, array('class' => 'form-control', "placeholder" => "98xxxxxxxxx","autofocus" => "true" ,"required"=>"required")) !!}
                    </div>

                    <div class="form-group">
                        <label for="email" class="color-primary">Type:</label>
                        <select name="type" id="type"
                                class=" form-group form-control form-control-line" required>
                            <option value="">Select Type</option>
                            <option value="5">Session</option>
                            <option value="2">Member</option>

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="password" class="color-primary">Password:</label>
                        <input class="form-control" placeholder="Password" id="password" required="required" name="password" type="password"     data-parsley-minlength="8"
                               data-parsley-number="1"     data-parsley-special="1"
                               value="">
                    </div>


                    <div class="form-group">
                        <label for="password" class="color-primary">Confirm Password:</label>
                        <input class="form-control" placeholder="Password Confirm" data-parsley-equalto="#password"     data-parsley-minlength="8"
                               name="password_confirmation" type="password" required value="">
                    </div>




                    <div class="form-group">
                        <label for="email" class="color-primary">Project:</label>
                        <select name="project_id" id="selectcategory"
                                class=" form-group form-control form-control-line" >
                            <option value="">Select Project</option>
                            @foreach($projects as $item)

                                <option value="{{$item->id}}">{{$item->name}}</option>

                            @endforeach


                        </select>
                    </div>

                    <div class="form-group">
                        {!! Form::submit( 'Apply', array('class' => 'btn btn-primary btn-wide')) !!}
                    </div>

                </form>


            </div>
        </div>
    </div>
</div>


{{--<script>--}}
    {{--$('#type').on('change', function() {--}}
        {{--if(this.value != 5 ){--}}

        {{--}--}}
    {{--})--}}
{{--</script>--}}
</body>
</html>