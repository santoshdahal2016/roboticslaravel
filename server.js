var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

var port = process.env.PORT || 5000;

server.listen(port, function () {
    console.log('Server listening at port %d', port);
});

app.get('/', function(req, res, next) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end('Hello World!');
});


io.on('connection', function(client) {
    client.emit('message', 'You are connected!');
    client.on('join', function(data) {
        console.log(data);
    });

    client.on('messages', function(data){
        client.emit('thread', data);
        client.broadcast.emit('thread', data);
    });
});

